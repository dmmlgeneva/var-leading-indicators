function prepTrainTest(fName,numLagsIn,trainLength,testLength,normalized,numFolds,sp)
% PREPTRAINTEST - prepare the train and test samples from the input and output data
%   and the innter cv splits of train data
%   will use the last part of the series as test and train data (and drop everything in the beginning which is not needed)
%   normalizes the data by the mean and std calculated over the train sample
%
% INPUTS
%   fName - name of file where the ts data are stored
%   numLagsIn - number of lags to use as inputs (default = 5)
%   trainLength - train length
%   testLength - test length
%   normalized - use normalized data = 1 (or not = 0)
%   numFolds - number of cross-validation folds (default = 5)
%   sp - single(=1) or double(0) precision
% OUTPUTS
%   saves genretad train/test samples in tt a ttNorm mat files
% 
% EXAMPLE: prepTrainTest('../expData/synth_5_3_1_1',5,30,500,1,5)
%
% CREATED: MG - 24/12/2016
%
% MODIFICATION HISTORY:
% MG 19/1/2017 - changed to store the samples in single precision

%% fill in optional arguments
if ~exist('trainLength','var') || isempty(trainLength),
  trainLength = 100;
end
if ~exist('testLength','var') || isempty(testLength),
  testLength = 500;
end
if ~exist('sp','var') || isempty(sp),
  sp = 0;
end

%% load data
load(fullfile(fName,['inOut',num2str(numLagsIn),'.mat']),'inData','outData');

%% split to train-test data
% train
endIdx = size(inData,1);
idxTrain = endIdx-testLength-trainLength+1:endIdx-testLength;
% test
idxTest = endIdx-testLength+1:endIdx;
if sp
  yTrain = single(outData(idxTrain,:));
  xTrain = single(inData(idxTrain,:));
  yTest = single(outData(idxTest,:));
  xTest = single(inData(idxTest,:));
else
  yTrain = outData(idxTrain,:);
  xTrain = inData(idxTrain,:);
  yTest = outData(idxTest,:);
  xTest = inData(idxTest,:);
end  

%% save or normalize and save
if ~normalized
  save(fullfile(fName,['tt',num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'.mat']),'yTrain','xTrain','yTest','xTest','numLagsIn')
else
  % normalize data using the training mean and std
  numTs = size(yTrain,2);
  % base the mean and std on the whole available history in train dataset
  yMean = mean([yTrain; xTrain(1:numLagsIn,1:numTs)]); yStd = std([yTrain; xTrain(1:numLagsIn,1:numTs)]);
  
  % use the same normalising constants for inputs and outputs
  yTrain = (yTrain - repmat(yMean,size(yTrain,1),1))./repmat(yStd,size(yTrain,1),1);
  yTest = (yTest - repmat(yMean,size(yTest,1),1))./repmat(yStd,size(yTest,1),1);
  xTrain = (xTrain - repmat(yMean,size(xTrain,1),numLagsIn))./repmat(yStd,size(xTrain,1),numLagsIn);
  xTest = (xTest - repmat(yMean,size(xTest,1),numLagsIn))./repmat(yStd,size(xTest,1),numLagsIn);

  % save normalized data
  save(fullfile(fName,['ttNorm',num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'.mat']),'yTrain','xTrain','yTest','xTest','numLagsIn','yMean','yStd')
end

%% split the train sample to k-folds cross validation samples
origY = yTrain;
origX = xTrain;
cvp = cvpartition(trainLength,'kfold',numFolds);
for cvI = 1:numFolds
  trainIdx = cvp.training(cvI);
  testIdx = cvp.test(cvI);
  yTrain = origY(trainIdx,:);
  xTrain = origX(trainIdx,:);
  yTest = origY(testIdx,:);
  xTest = origX(testIdx,:);
  % and save
  if normalized 
    save(fullfile(fName,['ttNorm',num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'_',num2str(cvI),'.mat']),'yTrain','xTrain','yTest','xTest','numLagsIn')
  else
    save(fullfile(fName,['tt',num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'_',num2str(cvI),'.mat']),'yTrain','xTrain','yTest','xTest','numLagsIn')
  end
end  

end