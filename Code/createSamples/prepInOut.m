function prepInOut(fName,numLagsIn)
% PREPINOUT - transform time series data into input and output data
%   expects to find rawData.mat with tsData variable within the folder fName 
%
% INPUTS
%   fName - folder name for the data
%   numLagsIn - number of lags to use as inputs (default = 5)
% OUTPUTS
%   saves genretad inData and outData into the fName folder intou inOutx.mat, where x is the numLagsIn
% 
% EXAMPLE: prepInOut('../expData/synth_5_3_1_1',5)
%
% CREATED: MG - 24/12/2016

%% fill in optional arguments
if ~exist('numLagsIn','var') || isempty(numLagsIn),
  numLagsIn = 5;
end

%% load data
load(fullfile(fName,'rawData.mat'),'tsData');

%% create input and output data
numTs = size(tsData,2);
outData = tsData((1+numLagsIn):end,:);
inData = zeros(length(tsData)-numLagsIn,numLagsIn*numTs);
%% this is brute force rearanging the inputs (no time to search elagant solution)
% the input will be [y_{t-1}, y_{t-2}, ...] where each y_t is the multivariate series
for idxLag = 1:numLagsIn
  inData(:,[numTs*(idxLag-1)+1:numTs*idxLag]) = tsData((1+numLagsIn-idxLag):(end-idxLag),:);
end

%% and save
% save the data
save(fullfile(fName,['inOut',num2str(numLagsIn),'.mat']),'inData','outData','numLagsIn')

end