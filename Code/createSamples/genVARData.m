function genVARData(fName,VARCoeffs,tsLength)
% GENVARDATA - generate VAR data from VARCoeffs
%
% INPUTS
%   fName - folder name to store the data
% OUTPUTS
%   saves genretad tsData into the fName file
% 
% EXAMPLE: genVARData('synth',varCoeffs,500)
%
% CREATED: MG - 24/12/2016

%% some global variables
numTs = size(VARCoeffs,1);
numLags = size(VARCoeffs,2)/numTs;
aCell = mat2cell(VARCoeffs,numTs,repmat(numTs,1,numLags));

%% generate random errors;
totalLength = tsLength*2+numLags; % generate much longer data so that can cut the initial burn-out
epsFull = mvnrnd(zeros(1,numTs),ones(1,numTs),totalLength);

%% inititate ts data
tsFull = zeros(totalLength,numTs);
tsFull(1:numLags,:) = epsFull(1:numLags,:);

%% generate data recursively
for recIdx = numLags+1:totalLength;
  tsNewTemp = zeros(numTs,1);
  for lagIdx = 1:numLags
    tsNewTemp = tsNewTemp + aCell{lagIdx}*tsFull(recIdx-lagIdx,:)';
  end
  tsFull(recIdx,:) = tsNewTemp' + epsFull(recIdx,:);
end

%% get the last bit of generated data
tsData = tsFull(end-tsLength+1:end,:);
epsData = epsFull(end-tsLength+1:end,:);

%% and save
% make sure fName includes final slash
slPos = max(strfind(fName,'/'));
if slPos <= length(fName)
  fName = [fName,'/'];
end
% make sure folder exists
if exist(fName,'dir') == 0
  mkdir(fName)
end
% save the data
save(fullfile(fName,'rawData.mat'),'VARCoeffs','tsData','epsData');

end


