function VARCoeffs = genVARCoeffs(numTs,numLags,numLeading)
% GENVARCOEFFS - generate stationary VAR coefficients 
%   a) either full VAR model (if numLeading not specified)
%   b) or sparse with leading indicators for all the other series
%   based on the properties companion matrix of the VAR(1) reformulation of the process (see Lutkhepol 2008, p15)
%
% INPUTS
%   numTs - number of ts
%   numLags - number of lags
% INPUTS OPTIONAL
%   numLeading - num of leading indicators, if not specified = full dependenc graph
% OUTPUTS
%   VARCoeffs - coefficients of stable VAR - numLags cell with (numTs x numTs) matrices: y_t = A_1*y_{t-1} + A_2 * y_{t-2} ... + e_t
% 
% EXAMPLE: A = genVARCoeffs(5,3,2)
%
% CREATED: MG - 22/12/2016

%% random eigenvalues (for stationarity, must have modulus smaller than 1)
eigVals = -0.9 + 1.8*rand(numTs*numLags,1);
eigVals = sort(eigVals,'descend');

%% random eigenvectors 
% first for rows corresponding to the coeff matrices
scale = 2; shift = -1; % scale and shift for the uniform distrib bellow; not sure how the values are really important, probably not a lot
if ~exist('numLeading','var') || isempty(numLeading) || numLeading == 0 % without leads
  eV = [];
  for nL = 1:numLags 
    % first the diagonals
    eVL = diag(shift + scale*rand(numTs,1));
    eV = [eV eVL];
  end
elseif numLeading == numTs
  eV = shift + scale*rand(numTs,numTs*numLags); % uniform distrib 
else % for leads
  leads = randsample(numTs,numLeading);
  eV = [];
  for nL = 1:numLags 
    % first the diagonals
    eVL = diag(shift + scale*rand(numTs,1));
    % next the leaders
    for l = [leads]'
      eVL(:,l) = shift + scale*rand(numTs,1);
    end
    eV = [eV eVL];
  end
end  
eigV{1} = eV; % top part of the eigvectors for full dependency
% now for the I matrix below in the companion matrix
for lagIdx = 2:numLags
  eigV{lagIdx}=eigV{lagIdx-1}./repmat(eigVals',numTs,1);
end

% normalize the eigvectors to l2 size 1
eigVecs = vertcat(eigV{:});
eigNorms = repmat(sqrt(sum(eigVecs.*eigVecs)),numTs*numLags,1);
eigVecs = eigVecs./eigNorms;
% and the get the final companion matrix from the SVD
invEigVecs = inv(eigVecs);
compMatrix = eigVecs*diag(eigVals)*invEigVecs;
%sort(eig(compMatrix))
VARCoeffs  =  compMatrix(1:numTs,:); % this should have params of a valid stable VAR
VARCoeffs(abs(VARCoeffs)<1e-6) = 0; % get rid of zeros

end  
  


