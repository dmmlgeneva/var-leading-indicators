function sumCV(fName)
% SUMCV - summarise the results across cv samples and find the best hyper-parameters
%
% INPUTS
%   fName - folder name to get the results from
% OUTPUTS
%   saves mseTestCV mseTestCVMean bestCVLbda into sumCV file
% 
% EXAMPLE: sumCV(fName)
%
% CREATED: MG - 24/12/2016


%% initiate output variables
mseTestCV = struct; mseTestCVMean = struct; bestCVLbda = struct;
fList = dir([fName,'*']); 
fList = {fList(2:end).name}';

%% get results from all the cv samples
for cvI = 1:numel(fList)
  load(fullfile([fName,'_',num2str(cvI)],'sumMSE.mat'),'mseTest')
  mseTe(cvI) = mseTest;
end

%% sumarise results of all methods across the cv samples
ff = fields(mseTe);
for mIdx = 1:numel(ff)
  metName = ff{mIdx};
  mseTemp = arrayfun(@(x) x.(metName), mseTe, 'UniformOutput',0);
  mseMean = mean([mseTemp{:}],2);
  [~,lbdIdx] = min(mseMean);
  mseTestCV = setfield(mseTestCV,metName,[mseTemp{:}]);
  mseTestCVMean = setfield(mseTestCVMean,metName,mseMean);
  bestCVLbda = setfield(bestCVLbda,metName,lbdIdx);
end

%% get the best MSE from the hold-out sample and the corresponding theta
bestMse = struct; bestLbda = struct; bestMseFromCV = struct; bestThetaFromCV = struct; bestTheta = struct; bestCVLbdaTime = struct; bestCVIters = struct;
% loop through all methods
load(fullfile(fName,'sumMSE.mat'),'mseTest','runTime','iters','relMseTest','baseModName')
load(fullfile(fName,'sumTheta.mat'),'Thetas')
ff = fields(mseTest);
for mIdx = 1:numel(ff)
  metName = ff{mIdx};
  mseTemp = arrayfun(@(x) x.(metName), mseTest, 'UniformOutput',0); % get the hold-out mseTest results of the method
  if iscell(mseTemp) mseT=[mseTemp{:}]; else mseT=mseTemp; end % if method with hyper-parameteres is cell otherwise scalar
  [mseMin,lbdBest] = min(mseT); % get the best lbda and mse over test
  bestMse = setfield(bestMse,metName,mseMin);
  bestLbda = setfield(bestLbda,metName,lbdBest);
  % do the same but using the cross-validated lambda
  if isfield(bestCVLbda,metName) lbdCV=bestCVLbda.(metName); else lbdCV=1; end; % if method without hyper-param fill in 1 as lbdCV
  bestMseFromCV = setfield(bestMseFromCV,metName,mseT(lbdCV)); % this is the final mse based on cross-validated lbda
  if isfield(runTime,metName)
    bestCVLbdaTime = setfield(bestCVLbdaTime,metName,runTime.(metName)(lbdCV)); % this is the time of the best CV lbda
  end
  if isfield(iters,metName)
    bestCVIters = setfield(bestCVIters,metName,iters.(metName)(lbdCV)); % this is the time of the best CV lbda
  end
  % get the corresponding thetas
  thetaTemp = Thetas.(metName);
  if iscell(thetaTemp) thB=thetaTemp{lbdBest}; thCV=thetaTemp{lbdCV}; else thB=thetaTemp; thCV=thetaTemp; end % if method with hyper-parameteres is cell otherwise scalar
  bestTheta = setfield(bestTheta,metName,thB);
  bestThetaFromCV = setfield(bestThetaFromCV,metName,thCV);
end


save(fullfile(fName,'sumCV.mat'),'mseTestCV','mseTestCVMean','bestCVLbda','bestMse','bestLbda','bestMseFromCV','bestTheta','bestThetaFromCV','bestCVLbdaTime','bestCVIters')

end