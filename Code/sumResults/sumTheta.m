function sumTheta(fName)
% SUMTHETA - summarise theta of all methods within an experiment
%
% INPUTS
%   fName - folder name to get the results from
% OUTPUTS
%   saves thetas into sumTheta matfile
% 
% EXAMPLE: sumTheta(fName)
%
% CREATED: MG - 25/12/2016


%% get list of all methods
metList = dir(fName); metList = {metList(3:end).name}'; % 3:end to get rid of '.' and '..'
Thetas = struct; 
for metIdx = 1:numel(metList)
  metName = metList{metIdx}; metName = metName(1:end-4);
  if isempty(strfind(metName,'sum')) % skip the summary files
    load(fullfile(fName,[metName,'.mat']),'theta')
    Thetas = setfield(Thetas,metName,theta);
  end
end

% save the data
save(fullfile(fName,'sumTheta.mat'),'Thetas');

end