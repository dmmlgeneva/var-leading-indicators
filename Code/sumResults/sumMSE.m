function sumMSE(fName)
% SUMMSE - summarise MSE of all methods within an experiment
%
% INPUTS
%   fName - folder name to get the results from
% OUTPUTS
%   saves mseTrain and mseTest structs into sumMSE matfile
% 
% EXAMPLE: sumMSE(fName)
%
% CREATED: MG - 24/12/2016

%% general list of possible grid list and the order in which they are used (manual setup so careful!)
gridList = {'lbdaGrid' 'kappaGrid' 'rnkGrid'};

%% get list of all methods
metList = dir(fName); metList = {metList(3:end).name}'; % 3:end to get rid of '.' and '..'
mseTrain = struct; mseTest = struct; grids = struct; runTime = struct; iters = struct;
for metIdx = 1:numel(metList)
  metName = metList{metIdx}; metName = metName(1:end-4);
  if isempty(strfind(metName,'sum')) % skip the summary files
    clear *Grid time numIter;
    load(fullfile(fName,[metName,'.mat']),'errTrain','errTest','*Grid','time','numIter')
    if exist('time','var')
      %fprintf('time %s\n',metName)
      runTime = setfield(runTime,metName,[time{:}]');
    end
    if exist('numIter','var')
      %fprintf('numIter %s\n',metName)
      iters = setfield(iters,metName,[numIter{:}]');
    end
    if ~iscell(errTrain) % models without hyper-parameters
      mseTrain = setfield(mseTrain,metName,mean(mean(errTrain.*errTrain)));
      mseTest = setfield(mseTest,metName,mean(mean(errTest.*errTest)));
    else
      mseTr = zeros(numel(errTrain),1); mseTe = zeros(numel(errTrain),1);
      % get the hyper-param grids (fairly complicated cause each method can be different)
      gr = who('*Grid'); expGr = cell(numel(gr),1);
      % load all the different grids into single cell
      for i = 1:numel(gr)
        g{i} = eval(sprintf(gridList{i}));
      end
      % expand the grids as if run through for-loop
      [expGr{:}] = ndgrid(g{1:numel(gr)});
      expGr = cellfun(@(x) x(:),expGr,'UniformOutput',0);
      grids = setfield(grids,metName,[expGr{1:numel(gr)}]);
      for lbdIdx=1:numel(errTrain)
        mseTr(lbdIdx) = mean(mean(errTrain{lbdIdx}.*errTrain{lbdIdx}));
        mseTe(lbdIdx) = mean(mean(errTest{lbdIdx}.*errTest{lbdIdx}));
      end
      mseTrain = setfield(mseTrain,metName,mseTr);
      mseTest = setfield(mseTest,metName,mseTe);
    end
  end
end

% save the data
save(fullfile(fName,'sumMSE.mat'),'mseTrain','mseTest','grids','runTime','iters');

end