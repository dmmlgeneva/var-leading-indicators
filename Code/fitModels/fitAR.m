function [predTrain,predTest,errTrain,errTest,theta] = fitAR(yTrain,xTrain,yTest,xTest)
% FITAR - fit simple linear AR model by OLS - each series uses only its own past (y1_t = <x1_t,theta1> )
%
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitAR(yTrain,xTrain,yTest,xTest)
%
% CREATED: MG - 24/12/2016

%% fit AR model
numTs = size(yTrain,2);
lenX = size(xTrain,2);
theta = zeros(lenX,numTs);
for tsIdx = 1:numTs
  xT = xTrain(:,[tsIdx:numTs:lenX]);
  theta([tsIdx:numTs:lenX],tsIdx) = xT\yTrain(:,tsIdx);
end
predTrain = xTrain*theta;
predTest = xTest*theta;
errTrain = yTrain-predTrain;
errTest = yTest-predTest;

end