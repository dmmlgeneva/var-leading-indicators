function [predTrain,predTest,errTrain,errTest,theta,time,numIter,objHistory] = fitVARLG(yTrain,xTrain,yTest,xTest,lbdaGrid,lbdaGridActive,initTheta,groupsTheta,updateTresh,maxIter)
% FITVARLG - fit simple linear VAR model with group-lasso penalty theta = argmin 1/n||Y-X theta||_F + lambda||theta||_G = argmin 1/(lambda*n) ||Y-X theta||_F + ||theta||_G
%   do it for the whole lambda grid
%   careful about the groups in your theta matrix - see groupsTheta
%   
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
%   lbdaGrid - grid for hyper-parameters
%   lbdaGridActive - 0/1 vector indicating the active part of the lbdaGrid for search (the rest of the grid is there just for compatibility)
%   initTheta - initial values of theta parameters (eg. from ridge solution)
%   groupsTheta - definition of groups in Theta (matrix of same size as theta with elements indicating group number)
%   updateTresh - if change in objective smaller than this, stop grad descent (more complicated, check code)
%   maxIter - max number of iterations
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitVARL1(yTrain,xTrain,yTest,xTest,lbdaGrid) 
%
% CREATED: MG - 1/1/2017
%
% MODIFICATION HISTORY:
% MG 19/1/2017 - changed to FISTA with backtracking Back&Taboulle2009

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-05;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 10000;
end

%% precalculate a few things;
[n,d] = size(xTrain);
XTX = xTrain'*xTrain;
XTy = xTrain'*yTrain;

% for the group functions;
uqGroupsTheta = [unique(groupsTheta)]';

%% function definitions
function gn = groupNorm(A)
  gn = 0; gn1=0;
  for gr = uqGroupsTheta
    grPos = find(groupsTheta==gr);
    gn = gn + norm(A(grPos),2); % tested, and using find is faster than replacing grPos by the logical vector
  end
  % gn = sum(arrayfun(@(x) norm(A(groupsTheta==x), uqGroupsTheta)); % for GPU ?
end
function gp = groupProx(A,aph)
  gp = zeros(size(A));
  for gr = uqGroupsTheta
    grPos = find(groupsTheta==gr);
    gp(grPos) = max( 1-aph/norm(A(grPos),2), 0) .* A(grPos); % tested, and using find is faster than replacing grPos by the logical vector
  end
end
%objFunc = @(Th,lb) 0.5/(n*lb)*norm(yTrain-xTrain*Th,'fro')^2 + groupNorm(Th);
%lFunc = @(Th,lb) 0.5/(n*lb)*norm(yTrain-xTrain*Th,'fro')^2;
function lF = lFunc(Th,lb) % loss function
  er = yTrain-xTrain*Th;
  lF = 0.5/(n*lb)*(er(:)'*er(:));
end
rFunc = @(Th) groupNorm(Th); % regularizer
lGrad = @(Th,lb) 1/(n*lb)*(-XTy + XTX*Th);
proxFunc = @(Th,alp) groupProx(Th,alp);

%% get the whole regularization path
lGrid = length(lbdaGrid);
theta = cell(lGrid,1); predTrain = cell(lGrid,1); predTest = cell(lGrid,1); errTrain = cell(lGrid,1); errTest = cell(lGrid,1);
time = cell(lGrid,1); numIter=cell(lGrid,1); objHistory=cell(lGrid,1);
% loop through the whole grid
for lbdIdx = [1:lGrid]
  lbda = lbdaGrid(lbdIdx);
  if lbdaGridActive(lbdIdx) == 1 % check wheather active part of grid
    %%% FISTA
    % initiate grad descent moitoring
    thetaLbda = initTheta{lbdIdx}; 
    iter=1;
    objHistory{lbdIdx}(iter,1) = lFunc(thetaLbda,lbda) + rFunc(thetaLbda);
    % initiate FISTA variables
    titer=1; 
    thetaY = thetaLbda;
    % line search variables (alpha is the estimate of the 1/L)
    alpha = 100; beta = 0.5; 
    % do the accelerated grad descent
    tLbda = tic;
    while iter<maxIter
      grad = lGrad(thetaY,lbda);
      % line search
      while alpha>1e-05
        thetaNew = proxFunc(thetaY - alpha*grad,alpha); % eq 4.1
        thetaDiff = thetaNew(:) - thetaY(:);
        if lFunc(thetaNew,lbda) <= lFunc(thetaY,lbda) + grad(:)'*thetaDiff + 0.5/alpha * (thetaDiff'*thetaDiff)
          tNew = 0.5 + sqrt(1+4*titer^2)/2; % eq 4.2
          thetaY = thetaNew + (titer-1)/tNew*(thetaNew-thetaLbda); % eq. 4.3
          titer = tNew; thetaLbda = thetaNew; % and update titer and thetaLbda
          break
        else
          alpha = alpha*beta;
        end
      end
      iter=iter+1; objHistory{lbdIdx}(iter,1) = lFunc(thetaLbda,lbda) + rFunc(thetaLbda);
      % check descent of algo  - don't do, not necessarily a descent algo
      %if objHistory{lbdIdx}(iter,1) > (objHistory{lbdIdx}(iter-1,1)+updateTresh)
      %  error('Error: not a descent step \n lbdIdx=%d, objValue at iter=%d : %12.8f , at iter-1=%d %12.8f \n', lbdIdx, iter, objHistory{lbdIdx}(iter,1), iter-1, objHistory{lbdIdx}(iter-1,1))
      %end
      % check convergence of grad descent
      if iter>10 && sum(objHistory{lbdIdx}(iter-10:iter-1)-objHistory{lbdIdx}(iter)) < updateTresh
        break
      end
    end
    numIter{lbdIdx} = iter;
    time{lbdIdx} = toc(tLbda);
    theta{lbdIdx} = thetaLbda;
		fprintf('FISTA fitVARLG lbdIdx=%d time: %6.2f \n',lbdIdx,time{lbdIdx})
  else % if not active part of grid
    numIter{lbdIdx} = 0;
    time{lbdIdx} = 0;
    theta{lbdIdx} = zeros(size(initTheta{lbdIdx}));
		fprintf('fitVARLG lbdIdx=%d time: %6.2f \n',lbdIdx,time{lbdIdx})
  end
% get the predictions and errors
  predTrain{lbdIdx} = xTrain*theta{lbdIdx};
  predTest{lbdIdx} = xTest*theta{lbdIdx};
  errTrain{lbdIdx} = yTrain-predTrain{lbdIdx};
  errTest{lbdIdx} = yTest-predTest{lbdIdx}; 
end

end
