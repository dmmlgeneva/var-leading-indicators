function [predTrain,predTest,errTrain,errTest,theta,time,numIter,objHistory,numIterSimplexAvg,timeSimplexAvg] = fitSCVAR(yTrain,xTrain,yTest,xTest,lbdaGrid,lbdaGridActive,kappaGrid,kappaGridActive,initTheta,updateTresh,maxIter)
% FITSCVAR - fit SCVAR model of Gregorova et al. Learning Leading Indicators for Time Series Predictions
%   
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
%   lbdaGrid,kappaGrid - grid for hyper-parameters
%   lbdaGridActive,kappaGridActive - 0/1 vector indicating the active part of the lbdaGrid for search (the rest of the grid is there just for compatibility)
%   initTheta - initial values of theta parameters (eg. from ridge solution)
%   updateTresh - if change in objective smaller than this, stop grad descent (more complicated, check code)
%   maxIter - max number of iterations
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitSCVAR(yTrain,xTrain,yTest,xTest,lbdaGrid,kpaGrid) 
%
% CREATED: MG - 1/1/2017


%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-05;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 10000;
end

%% precalculate a few things;
[n,K] = size(yTrain); % train length and number of time series
d = size(xTrain,2); % number of parameters
p = d/K; % number of lags
R = zeros(n,K); % prealocating space
% create the xCell (each cell has all the past values of a single series)
xCell = squeeze(mat2cell(permute(reshape(xTrain,n,K,[]),[1,3,2]),n,p,ones(1,K)));



%% function definitions
getA = @(alp) repmat(alp,1,K); % create A matrix
getGamma = @(a) a - diag(diag(a)) + eye(size(a));  % create Gamma matrix
getW = @(g,v) repmat(g,p,1) .* v; % get W from Gamma and V
getZ = @(g,tsIdx) repmat([g(:,tsIdx)]',n,p) .* xTrain; % get Z from Gamma and X
getVCell = @(vv) mat2cell(reshape(vv,K,[])',p,ones(1,K))'; % get the VCell needed to get H
getH = @(vCol) cellfun(@(x,y) x*y, xCell,getVCell(vCol),'Uniformoutput',0); % get H from a column of V and X
objFunc = @(g,v,lb) 0.5/(n*lb)*norm(yTrain-xTrain*getW(g,v),'fro')^2 + 0.5*(v(:)'*v(:)); % the objective function 

%% get the whole regularization path
lGrid = length(lbdaGrid); kGrid = length(kappaGrid);
% initiate cell variables
theta = cell(lGrid,kGrid); gamma = cell(lGrid,kGrid); 
predTrain = cell(lGrid,kGrid); predTest = cell(lGrid,kGrid); errTrain = cell(lGrid,kGrid); errTest = cell(lGrid,kGrid);
time = cell(lGrid,kGrid); numIter=cell(lGrid,kGrid); objHistory=cell(lGrid,kGrid);
timeSimplexAvg = cell(lGrid,kGrid); numIterSimplexAvg=cell(lGrid,kGrid);
% loop through the whole grid
for lbdIdx = [1:lGrid]
  for kpaIdx = [1:kGrid]
    lbda = lbdaGrid(lbdIdx);
    kappa = kappaGrid(kpaIdx);
    if lbdaGridActive(lbdIdx) == 1 && kappaGridActive(kpaIdx) % check wheather active part of grid   
      ridgeMat = n*lbda*eye(d);
      % initiate grad descent moitoring
      iter=1; Gamma = kappa/K*ones(K) + (1-kappa/K)*eye(K);
      V = initTheta{lbdIdx} ./ repmat(Gamma,p,1); 
      avgIterSimplex = 0; avgTimeSimplex = 0;
      alpha = kappa/K*ones(K,1);
      objHistory{lbdIdx,kpaIdx}(iter,1) = objFunc(Gamma,V,lbda);
      %objFunc(Gamma,V,lbda)
      % do the alternating grad descent as in algo 1
      tLbda = tic;
      while iter<maxIter
        % step 1: solve for V
        for tsIdx = 1:K
          Z = getZ(Gamma,tsIdx); % get specific Z per ts
          V(:,tsIdx) = (Z'*Z + ridgeMat)\Z'*yTrain(:,tsIdx); % solve the ridge problem per ts
        end
        %objFunc(Gamma,V,lbda)

        % step 2: solve for alpha
        H = [];
        for tsIdx = 1:K
          Hk = getH(V(:,tsIdx)); % get specific time series Hk
          R(:,tsIdx) = yTrain(:,tsIdx) - Hk{tsIdx}; % get residuals from fitting on own history
          Hk{tsIdx} = zeros(n,1); % zero out own history params
          H = [H; [Hk{:}] ]; % concatenate Hk into a common H
        end
        [alpha,iterSimplex,timeSimplex] = simplexLS(R(:),H,alpha,kappa); % solve the constraint least squares problem

        % create Gamma and get W
        Gamma = getGamma(getA(alpha));
        %objFunc(Gamma,V,lbda)
        iter=iter+1; objHistory{lbdIdx,kpaIdx}(iter,1) = objFunc(Gamma,V,lbda);
        avgIterSimplex = avgIterSimplex + iterSimplex;
        avgTimeSimplex = avgTimeSimplex + timeSimplex;
        % check descent of algo - was here to test the algo but may not be hold exactly with single precision so not use
        % if objHistory{lbdIdx,kpaIdx}(iter,1) > (objHistory{lbdIdx,kpaIdx}(iter-1,1)+updateTresh)
        %   error('Error: not a descent step \n lbdaIdx=%d, kappaIdx=%d, \n objValue at iter=%d : %12.8f , at iter-1=%d %12.8f \n', lbdIdx, kpaIdx, iter, objHistory{lbdIdx}(iter,1), iter-1, objHistory{lbdIdx}(iter-1,1))
        % end
        % check convergence of grad descent
        if iter>5 && sum(objHistory{lbdIdx,kpaIdx}(iter-5:iter-1)-objHistory{lbdIdx,kpaIdx}(iter)) < updateTresh
          break
        end
      end
      % get output variables
      numIter{lbdIdx,kpaIdx} = iter;
      time{lbdIdx,kpaIdx} = toc(tLbda);
      fprintf('fitSCVAR lbdIdx=%d, kpaIdx=%d time: %6.2f \n',lbdIdx,kpaIdx,time{lbdIdx,kpaIdx})
      numIterSimplexAvg{lbdIdx,kpaIdx} = avgIterSimplex/iter;
      timeSimplexAvg{lbdIdx,kpaIdx} = avgTimeSimplex/iter;
      theta{lbdIdx,kpaIdx} = getW(Gamma,V);
      gamma{lbdIdx,kpaIdx} = Gamma;
    else  % if not active part of grid
      % fill-in output variables
      numIter{lbdIdx,kpaIdx} = 0;
      time{lbdIdx,kpaIdx} = 0;
      fprintf('fitSCVAR lbdIdx=%d, kpaIdx=%d time: %6.2f \n',lbdIdx,kpaIdx,time{lbdIdx,kpaIdx})
      numIterSimplexAvg{lbdIdx,kpaIdx} = 0;
      timeSimplexAvg{lbdIdx,kpaIdx} = 0;
      theta{lbdIdx,kpaIdx} = initTheta{lbdIdx};
      gamma{lbdIdx,kpaIdx} = ones(K);
    end
    % get the predictions and errors
    predTrain{lbdIdx,kpaIdx} = xTrain*theta{lbdIdx,kpaIdx};
    predTest{lbdIdx,kpaIdx} = xTest*theta{lbdIdx,kpaIdx};
    errTrain{lbdIdx,kpaIdx} = yTrain-predTrain{lbdIdx,kpaIdx};
    errTest{lbdIdx,kpaIdx} = yTest-predTest{lbdIdx,kpaIdx}; 
  end
end

end
