function [predTrain,predTest,errTrain,errTest,theta] = fitTRUE(yTrain,xTrain,yTest,xTest,VARCoeffs,yMean,yStd)
% FITTRUE - fit the TRUE model
%
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
%   VARCoeffs - VAR coefficients
% INPUTS OPTIONAL
%   yMean,yStd - normalising mean and std
% OUTPUTS
%  predTrain,predTest - train and test predictions
%  errTrain,errTest - train and test errors
%  theta - the TRUE coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest] = fitTRUE(yTrain,xTrain,yTest,xTest)
%
% CREATED: MG - 13/01/2017

%% fill in optional arguments
if ~exist('yMean','var') || isempty(yMean),
  yMean = 0;
end
if ~exist('yStd','var') || isempty(yStd),
  yStd = 1;
end

%% fit TRUE model
numTs = size(yTrain,2);
numLags = size(VARCoeffs,2)/numTs;
lenX = size(xTrain,2);
%% reformat the VARCoeffs theta
tCell = mat2cell(VARCoeffs,numTs,repmat(numTs,1,numLags));
theta = [vertcat([tCell{:}]'); zeros(lenX - numTs*numLags,numTs)];
% get original (non-normalised) input data
xTr = xTrain.*repmat(yStd,size(xTrain,1),lenX/numTs)+repmat(yMean,size(xTrain,1),lenX/numTs);
xTe = xTest.*repmat(yStd,size(xTest,1),lenX/numTs)+repmat(yMean,size(xTest,1),lenX/numTs);
% get the non-normalised predictions
predTrain = xTr*theta;
predTest = xTe*theta;
% normalize the predictions
predTrain = (predTrain - repmat(yMean,size(predTrain,1),1))./repmat(yStd,size(predTrain,1),1);
predTest = (predTest - repmat(yMean,size(predTest,1),1))./repmat(yStd,size(predTest,1),1);
% and get errors
errTrain = yTrain-predTrain;
errTest = yTest-predTest;

end