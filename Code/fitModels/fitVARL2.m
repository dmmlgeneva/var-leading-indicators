function [predTrain,predTest,errTrain,errTest,theta,time] = fitVARL2(yTrain,xTrain,yTest,xTest,lbdaGrid) 
% FITVARL2 - fit simple linear VAR model with ridge penalty theta = argmin 1/n||Y-X theta||_F + lambda||theta||_F
%   do it for the whole lambda grid 
%   
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
%   lbdaGrid - grid for hyper-parameters
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitVARL2(yTrain,xTrain,yTest,xTest,lbdaGrid) 
%
% CREATED: MG - 24/12/2016

%% precalculate a few things;
[n,d] = size(xTrain);
XTX = xTrain'*xTrain;
XTy = xTrain'*yTrain;

%% get the whole regularization path
lGrid = length(lbdaGrid);
theta = cell(lGrid,1); predTrain = cell(lGrid,1); predTest = cell(lGrid,1); errTrain = cell(lGrid,1); errTest = cell(lGrid,1);
% loop through the whole grid
for lbdIdx = [1:lGrid]
  lbda = lbdaGrid(lbdIdx);  
  % solve by ridge
  tic;
  theta{lbdIdx} = (XTX + n*lbda*eye(d))\XTy;
  time{lbdIdx} = toc;
  % get the predictions and errors
  predTrain{lbdIdx} = xTrain*theta{lbdIdx};
  predTest{lbdIdx} = xTest*theta{lbdIdx};
  errTrain{lbdIdx} = yTrain-predTrain{lbdIdx};
  errTest{lbdIdx} = yTest-predTest{lbdIdx}; 
end

end
