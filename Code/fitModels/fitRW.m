function [predTrain,predTest,errTrain,errTest,theta] = fitRW(yTrain,xTrain,yTest,xTest)
% FITRW - fit simple random walk model
%
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
% OUTPUTS
%  predTrain,predTest - train and test predictions
%  errTrain,errTest - train and test errors
%  theta - random walk coefficients (eye)
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest] = fitRW(yTrain,xTrain,yTest,xTest)
%
% CREATED: MG - 24/12/2016

%% fit RW model
numTs = size(yTrain,2);
lenX = size(xTrain,2);
theta = [eye(numTs); zeros(lenX-numTs,numTs)];
predTrain = xTrain*theta;
predTest = xTest*theta;
errTrain = yTrain-predTrain;
errTest = yTest-predTest;

end