function [predTrain,predTest,errTrain,errTest,m] = fitMEAN(yTrain,xTrain,yTest,xTest)
% FITMEAN - fit simple Mean to data
%
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
% OUTPUTS
%  predTrain,predTest - train and test predictions
%  errTrain,errTest - train and test errors
%  m - mean over train data
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest] = fitMean(yTrain,xTrain,yTest,xTest)
%
% CREATED: MG - 24/12/2016

%% fit Mean model
m = mean(yTrain,1);
trainLength = size(yTrain,1);
testLength = size(yTest,1);
predTrain = repmat(m,trainLength,1);
predTest = repmat(m,testLength,1);
errTrain = yTrain-predTrain;
errTest = yTest-predTest;

end