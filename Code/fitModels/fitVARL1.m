function [predTrain,predTest,errTrain,errTest,theta,time,numIter,objHistory] = fitVARL1(yTrain,xTrain,yTest,xTest,lbdaGrid,lbdaGridActive,initTheta,updateTresh,maxIter)
% FITVARL1 - fit simple linear VAR model with l1 penalty theta = argmin 1/n||Y-X theta||_F + lambda||theta||_1 = argmin 1/(lambda*n) ||Y-X theta||_F + ||theta||_1
%   do it for the whole lambda grid 
%   
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
%   lbdaGrid - grid for hyper-parameters
%   lbdaGridActive - 0/1 vector indicating the active part of the lbdaGrid for search (the rest of the grid is there just for compatibility)
%   initTheta - initial values of theta parameters (eg. from ridge solution)
%   updateTresh - if change in objective smaller than this, stop grad descent (more complicated, check code)
%   maxIter - max number of iterations
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitVARL1(yTrain,xTrain,yTest,xTest,lbdaGrid) 
%
% CREATED: MG - 1/1/2017
%
% MODIFICATION HISTORY:
% MG 19/1/2017 - changed to FISTA with backtracking Back&Taboulle2009

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-05;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 10000;
end

%% precalculate a few things;
[n,d] = size(xTrain);
XTX = xTrain'*xTrain;
XTy = xTrain'*yTrain;

%% function definitions
%objFunc = @(Th,lb) 0.5/(n*lb)*norm(yTrain-xTrain*Th,'fro')^2 + norm(Th(:),1);
function lF = lFunc(Th,lb) % loss function
  er = yTrain-xTrain*Th;
  lF = 0.5/(n*lb)*(er(:)'*er(:));
end
rFunc = @(Th) norm(Th(:),1); % regularizer
%lFunc = @(Th,lb) 0.5/(n*lb)*norm(yTrain-xTrain*Th,'fro')^2;
lGrad = @(Th,lb) 1/(n*lb)*(-XTy + XTX*Th);
proxFunc = @(Th,alp) max( abs(Th)-alp, 0 ) .* sign(Th);

%% get the whole regularization path
lGrid = length(lbdaGrid);
theta = cell(lGrid,1); predTrain = cell(lGrid,1); predTest = cell(lGrid,1); errTrain = cell(lGrid,1); errTest = cell(lGrid,1);
time = cell(lGrid,1); numIter=cell(lGrid,1); objHistory=cell(lGrid,1);
% loop through the whole grid
for lbdIdx = [1:lGrid]
  lbda = lbdaGrid(lbdIdx);
  if lbdaGridActive(lbdIdx) == 1 % check wheather active part of grid
    %%% FISTA
    % initiate grad descent moitoring
    thetaLbda = initTheta{lbdIdx}; 
    iter=1;
    objHistory{lbdIdx}(iter,1) = lFunc(thetaLbda,lbda) + rFunc(thetaLbda);
    % initiate FISTA variables
    titer=1; 
    thetaY = thetaLbda;
    % line search variables (alpha is the estimate of the 1/L)
    alpha = 100; beta = 0.5; 
    % do the accelerated grad descent
    tLbda = tic;
    while iter<maxIter
      grad = lGrad(thetaY,lbda);
      % line search
      while alpha>1e-05
        thetaNew = proxFunc(thetaY - alpha*grad,alpha); % eq 4.1
        thetaDiff = thetaNew(:) - thetaY(:);
        if lFunc(thetaNew,lbda) <= lFunc(thetaY,lbda) + grad(:)'*thetaDiff + 0.5/alpha * (thetaDiff'*thetaDiff)
          tNew = 0.5 + sqrt(1+4*titer^2)/2; % eq 4.2
          thetaY = thetaNew + (titer-1)/tNew*(thetaNew-thetaLbda); % eq. 4.3
          titer = tNew; thetaLbda = thetaNew; % and update titer and thetaLbda
          break
        else
          alpha = alpha*beta;
        end
      end
      iter=iter+1; objHistory{lbdIdx}(iter,1) = lFunc(thetaLbda,lbda) + rFunc(thetaLbda);
      % check descent of algo - don't do, not necessarily a descent algo
      %if objHistory{lbdIdx}(iter,1) > (objHistory{lbdIdx}(iter-1,1)+updateTresh)
      %  error('Error: not a descent step \n lbdIdx=%d, objValue at iter=%d : %12.8f , at iter-1=%d %12.8f \n', lbdIdx, iter, objHistory{lbdIdx}(iter,1), iter-1, objHistory{lbdIdx}(iter-1,1))
      %end
      % check convergence of grad descent
      if iter>10 && sum(objHistory{lbdIdx}(iter-10:iter-1)-objHistory{lbdIdx}(iter)) < updateTresh
        break
      end
    end
    numIter{lbdIdx} = iter;
    time{lbdIdx} = toc(tLbda);
    theta{lbdIdx} = thetaLbda;
 		fprintf('FISTA fitVARL1 lbdIdx=%d time: %6.2f \n',lbdIdx,time{lbdIdx})  
  else % if not active part of grid
    numIter{lbdIdx} = 0;
    time{lbdIdx} = 0;
    theta{lbdIdx} = zeros(size(initTheta{lbdIdx}));
 		fprintf('fitVARL1 lbdIdx=%d time: %6.2f \n',lbdIdx,time{lbdIdx})
  end
  % get the predictions and errors
  predTrain{lbdIdx} = xTrain*theta{lbdIdx};
  predTest{lbdIdx} = xTest*theta{lbdIdx};
  errTrain{lbdIdx} = yTrain-predTrain{lbdIdx};
  errTest{lbdIdx} = yTest-predTest{lbdIdx}; 
end

end
