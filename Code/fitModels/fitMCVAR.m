function [predTrain,predTest,errTrain,errTest,theta,dMat,gMat,time,numIter,objHistory,numIterSimplexAvg,timeSimplexAvg,numIterSimplexAvgM,timeSimplexAvgM] = fitMCVAR(yTrain,xTrain,yTest,xTest,lbdaGrid,lbdaGridActive,kappaGrid,kappaGridActive,rnkGrid,rnkGridActive,initTheta,updateTresh,maxIter)
% FITMCVAR - fit MCVAR model of Gregorova et al. Learning Leading Indicators for Time Series Predictions
%   
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
%   lbdaGrid,kappaGrid,rnkGrid - grid for hyper-parameters
%   lbdaGridActive,kappaGridActive,rnkActive - 0/1 vector indicating the active part of the lbdaGrid for search (the rest of the grid is there just for compatibility)
%   initTheta - initial values of theta parameters (eg. from ridge solution)
%   updateTresh - if change in objective smaller than this, stop grad descent (more complicated, check code)
%   maxIter - max number of iterations
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
%   D,G - decomposition matrices
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitVARL1(yTrain,xTrain,yTest,xTest,lbdaGrid) 
%
% CREATED: MG - 1/1/2017

%% fill in optional arguments
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-05;
end
if ~exist('maxIter','var') || isempty(maxIter),
  maxIter = 10000;
end

%% precalculate a few things;
[n,K] = size(yTrain); % train length and number of time series
d = size(xTrain,2); % number of parameters
p = d/K; % number of lags
R = zeros(n,K); % prealocating space
% create the xCell (each cell has all the past values of a single series)
xCell = squeeze(mat2cell(permute(reshape(xTrain,n,K,[]),[1,3,2]),n,p,ones(1,K)));



%% function definitions
getA = @(d,g) d*g; % create A matrix
getGamma = @(a) a - diag(diag(a)) + eye(size(a));  % create Gamma matrix
getW = @(g,v) repmat(g,p,1) .* v; % get W from Gamma and V
getZ = @(g,tsIdx) repmat([g(:,tsIdx)]',n,p) .* xTrain; % get Z from Gamma and X
getVCell = @(vv) mat2cell(reshape(vv,K,[])',p,ones(1,K))'; % get the VCell needed to get H
getH = @(vCol) cellfun(@(x,y) x*y, xCell,getVCell(vCol),'Uniformoutput',0); % get H from a column of V and X
objFunc = @(g,v,lb) 0.5/(n*lb)*norm(yTrain-xTrain*getW(g,v),'fro')^2 + 0.5*(v(:)'*v(:)); % the objective function 

%% get the whole regularization path
lGrid = length(lbdaGrid); kGrid = length(kappaGrid); rGrid = length(rnkGrid);
% initiate cell variables
theta = cell(lGrid,kGrid,rGrid); gamma = cell(lGrid,kGrid,rGrid); 
predTrain = cell(lGrid,kGrid,rGrid); predTest = cell(lGrid,kGrid,rGrid); errTrain = cell(lGrid,kGrid,rGrid); errTest = cell(lGrid,kGrid,rGrid);
time = cell(lGrid,kGrid,rGrid); numIter=cell(lGrid,kGrid,rGrid); objHistory=cell(lGrid,kGrid,rGrid);
timeSimplexAvg = cell(lGrid,kGrid,rGrid); numIterSimplexAvg=cell(lGrid,kGrid,rGrid);
timeSimplexAvgM = cell(lGrid,kGrid,rGrid); numIterSimplexAvgM=cell(lGrid,kGrid,rGrid);
dMat = cell(lGrid,kGrid,rGrid); gMat=cell(lGrid,kGrid,rGrid);
% loop through the whole grid
for lbdIdx = [1:lGrid]
  for kpaIdx = [1:kGrid]
  for rnkIdx = [1:rGrid]
    lbda = lbdaGrid(lbdIdx); kappa = kappaGrid(kpaIdx); rnk = rnkGrid(rnkIdx);
    if lbdaGridActive(lbdIdx) == 1 && kappaGridActive(kpaIdx) && rnkGridActive(rnkIdx) % check wheather active part of grid   
      ridgeMat = n*lbda*eye(d);
      % initiate grad descent moitoring
      iter=1; D = kappa/K*ones(K,rnk); G = 1/rnk*ones(rnk,K);
      Gamma = getGamma(getA(D,G));
      V = initTheta{lbdIdx} ./ repmat(Gamma,p,1); 
      avgIterSimplex = 0; avgTimeSimplex = 0; avgIterSimplexM = 0; avgTimeSimplexM = 0;
      objHistory{lbdIdx,kpaIdx,rnkIdx}(iter,1) = objFunc(Gamma,V,lbda);
      %objFunc(Gamma,V,lbda)
      % do the alternating grad descent as in algo 1
      tLbda = tic;
      while iter<maxIter
        % step 1: solve for V
        for tsIdx = 1:K
          Z = getZ(Gamma,tsIdx); % get specific Z per ts
          V(:,tsIdx) = (Z'*Z + ridgeMat)\Z'*yTrain(:,tsIdx); % solve the ridge problem per ts
        end
        %objFunc(Gamma,V,lbda)
        
        % step 2a: solve for G
        H = [];
        for tsIdx = 1:K
          Hk = getH(V(:,tsIdx)); % get specific time series Hk
          R(:,tsIdx) = yTrain(:,tsIdx) - Hk{tsIdx}; % get residuals from fitting on own history
          Hk{tsIdx} = zeros(n,1); % zero out own history params
          H = [H; [Hk{:}] ]; % concatenate Hk into a common H
          [G(:,tsIdx),iterSimplex,timeSimplex] = simplexLS(R(:,tsIdx),[Hk{:}]*D,G(:,tsIdx),1); % solve the constraint least squares problem
        end
        %Gamma = getGamma(getA(D,G));
        %objFunc(Gamma,V,lbda)
        
        % step 2b: solve for D
        % construct the HG matrices
        GH = kron(G',ones(n,K)) .* kron(ones(1,rnk),H);
        [D,iterSimplexM,timeSimplexM] = simplexLSMat(R,GH,D,kappa); % solve the constraint least squares problem

        % create Gamma and get W
        Gamma = getGamma(getA(D,G));
        %objFunc(Gamma,V,lbda)
        iter=iter+1; objHistory{lbdIdx,kpaIdx,rnkIdx}(iter,1) = objFunc(Gamma,V,lbda);
        % get the simplex times for monitoring
        avgIterSimplex = avgIterSimplex + iterSimplex;
        avgTimeSimplex = avgTimeSimplex + timeSimplex;
        avgIterSimplexM = avgIterSimplexM + iterSimplexM;
        avgTimeSimplexM = avgTimeSimplexM + timeSimplexM;
        % check descent of algo - was here to test the algo but may not be hold exactly with single precision so not use
        % if objHistory{lbdIdx,kpaIdx,rnkIdx}(iter,1) > (objHistory{lbdIdx,kpaIdx,rnkIdx}(iter-1,1)+updateTresh)
        %   error('Error: not a descent step \n lbdaIdx=%d, kappaIdx=%d, \n objValue at iter=%d : %12.8f , at iter-1=%d %12.8f \n', lbdIdx, kpaIdx, iter, objHistory{lbdIdx}(iter,1), iter-1, objHistory{lbdIdx}(iter-1,1))
        % end
        % check convergence of grad descent
        if iter>5 && sum(objHistory{lbdIdx,kpaIdx,rnkIdx}(iter-5:iter-1)-objHistory{lbdIdx,kpaIdx,rnkIdx}(iter)) < updateTresh
          break
        end
      end
      % prepare all output variables
      numIter{lbdIdx,kpaIdx,rnkIdx} = iter;
      time{lbdIdx,kpaIdx,rnkIdx} = toc(tLbda);
      fprintf('fitMCVAR lbdIdx=%d, kpaIdx=%d, rnkIdx=%d time: %6.2f \n',lbdIdx,kpaIdx,rnkIdx,time{lbdIdx,kpaIdx,rnkIdx})
      numIterSimplexAvg{lbdIdx,kpaIdx,rnkIdx} = avgIterSimplex/(iter*K);
      timeSimplexAvg{lbdIdx,kpaIdx,rnkIdx} = avgTimeSimplex/(iter*K);
      numIterSimplexAvgM{lbdIdx,kpaIdx,rnkIdx} = avgIterSimplexM/iter;
      timeSimplexAvgM{lbdIdx,kpaIdx,rnkIdx} = avgTimeSimplexM/iter;
      theta{lbdIdx,kpaIdx,rnkIdx} = getW(Gamma,V);
      gamma{lbdIdx,kpaIdx,rnkIdx} = Gamma;
      dMat{lbdIdx,kpaIdx,rnkIdx} = D; gMat{lbdIdx,kpaIdx,rnkIdx} = G;
    else % if not active part of grid
      % fill-in all output variables
      numIter{lbdIdx,kpaIdx,rnkIdx} = 0;
      time{lbdIdx,kpaIdx,rnkIdx} = 0;
      fprintf('fitMCVAR lbdIdx=%d, kpaIdx=%d, rnkIdx=%d time: %6.2f \n',lbdIdx,kpaIdx,rnkIdx,time{lbdIdx,kpaIdx,rnkIdx})
      numIterSimplexAvg{lbdIdx,kpaIdx,rnkIdx} = 0;
      timeSimplexAvg{lbdIdx,kpaIdx,rnkIdx} = 0;
      numIterSimplexAvgM{lbdIdx,kpaIdx,rnkIdx} = 0;
      timeSimplexAvgM{lbdIdx,kpaIdx,rnkIdx} = 0;
      theta{lbdIdx,kpaIdx,rnkIdx} = initTheta{lbdIdx};
      gamma{lbdIdx,kpaIdx,rnkIdx} = ones(K);
      dMat{lbdIdx,kpaIdx,rnkIdx} = ones(K,rnk); gMat{lbdIdx,kpaIdx,rnkIdx} = ones(rnk,K);
    end
    % get the predictions and errors
    predTrain{lbdIdx,kpaIdx,rnkIdx} = xTrain*theta{lbdIdx,kpaIdx,rnkIdx};
    predTest{lbdIdx,kpaIdx,rnkIdx} = xTest*theta{lbdIdx,kpaIdx,rnkIdx};
    errTrain{lbdIdx,kpaIdx,rnkIdx} = yTrain-predTrain{lbdIdx,kpaIdx,rnkIdx};
    errTest{lbdIdx,kpaIdx,rnkIdx} = yTest-predTest{lbdIdx,kpaIdx,rnkIdx}; 
  end
  end
end

end
