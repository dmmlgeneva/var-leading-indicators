function [theta,iter,time] = simplexLS(y,X,initTheta,sSize,maxIter,updateTresh)
% SIMPLEXLS a utility funciton for solving least squares with simplex constraints
%   solved by projected gradient descent using FISTA
%
% INPUTS
%   y - input vector
%   X - output matrix
%   initTheta - initial value for theta vector (usully coming from ridge)
% INPUTS OPTIONAL
%   sSize - size of the simplex (default = 1)
%   maxIter - max number of iterations (default 1000)
%   updateTresh - stopping criterion for loss / params difference (default 1e-05)
% OUTPUTS
%   theta - parameters vector on simplex
% 
% EXAMPLE: theta = simplexLS(y,X,initTheta,kappa)
%
% CREATED: MG - 2/1/2017
%
% MODIFICATION HISTORY:
% MG 19/1/2017 - changed to FISTA with backtracking Back&Taboulle2009

%% fill in optional arguments
if ~exist('maxIter','var') || isempty(maxIter),
  %maxIter = 1000;
  maxIter = 100;
end
if ~exist('updateTresh','var') || isempty(updateTresh),
  updateTresh = 1e-05;
end
if ~exist('sSize','var') || isempty(sSize),
  sSize = 1;
end

%% precalculate a few things;
[n,d] = size(X);
XTX = X'*X;
XTy = X'*y;

%% function definitions
objFunc = @(Th) 0.5/n*norm(y-X*Th,2)^2;
objGrad = @(Th) 1/n*(-XTy + XTX*Th);

% initiate grad descent moitoring
theta = initTheta; iter=1;
objHistory(iter,1) = objFunc(theta);
% initiate FISTA variables
titer=1; 
thetaY = theta;
alpha = 100; beta = 0.5;
% do the accelerated grad descent
tSimplex = tic;
while iter<maxIter
  grad = objGrad(thetaY);
  % line search
  while alpha>1e-05
    thetaNew = simplexProject(thetaY - alpha*grad,sSize);
    thetaDiff = thetaNew - thetaY;
    if objFunc(thetaNew) <= objFunc(thetaY) + grad'*thetaDiff + 0.5/alpha * (thetaDiff'*thetaDiff)
      tNew = 0.5 + sqrt(1+4*titer^2)/2; % eq 4.2
      thetaY = thetaNew + (titer-1)/tNew*(thetaNew-theta); % eq. 4.3
      titer = tNew; theta = thetaNew; % and update titer and thetaLbda
      break
    else
      alpha = alpha*beta;
    end
  end
  iter=iter+1; objHistory(iter,1) = objFunc(theta);
  % check descent of algo - don't do, not necessarily a descent algo
  % if objHistory(iter,1) > (objHistory(iter-1,1)+updateTresh)
  %  error('Error: not a descent step \n objValue at iter=%d : %12.8f , at iter-1=%d %12.8f \n', iter, objHistory(iter,1), iter-1, objHistory(iter-1,1))
  % end
  % check convergence of grad descent
  if iter>5 && sum(objHistory(iter-5:iter-1)-objHistory(iter)) < updateTresh
    break
  end
end
time = toc(tSimplex);

end
