function outVec = simplexProject(inVec,sSize)
% SIMPLEXPROJECT a utility function to project on simplex (if input is matrix do this by columns)
% Chen,Ye (2011): Projection onto a simplex
%
% CREATED: MG - 10/6/2014
%
% MODIFICATION HISTORY:
% MG 19/1/2017 - changed using the code 
% (c) Xiaojing Ye Jan. 14, 2011.
% xyex19@gmail.com
%
% Algorithm is explained as in the linked document
% http://arxiv.org/abs/1101.6081
% or
% http://ufdc.ufl.edu/IR00000353/

if ~exist('sSize','var') || isempty(sSize),
  sSize = 1;
end

numCols = size(inVec,2);
numEl = size(inVec,1);
outVec = zeros(numEl,numCols);

% project each column on simplex
for colIdx=1:numCols
  y = inVec(:,colIdx);
  m = length(y); bget = false;

  s = sort(y,'descend'); tmpsum = 0;

  for ii = 1:m-1
      tmpsum = tmpsum + s(ii);
      tmax = (tmpsum - sSize)/ii;
      if tmax >= s(ii+1)
          bget = true;
          break;
      end
  end
    
  if ~bget, tmax = (tmpsum + s(m) -sSize)/m; end;

  outVec(:,colIdx) = max(y-tmax,0);

end


end
