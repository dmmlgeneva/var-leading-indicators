function [predTrain,predTest,errTrain,errTest,theta] = fitVAR(yTrain,xTrain,yTest,xTest)
% FITVAR - fit simple linear VAR model by OLS (y_t = <x_t,theta> )
%
% INPUTS
%   yTrain,xTrain - output,input train sample
%   yTest,xTest - output,input test sample
% OUTPUTS
%   predTrain,predTest - train and test predictions
%   errTrain,errTest - train and test errors
%   theta - linear model coefficients
% 
% EXAMPLE: [predTrain,predTest,errTrain,errTest,theta] = fitVAR(yTrain,xTrain,yTest,xTest)
%
% CREATED: MG - 24/12/2016

%% fit VAR model
theta = xTrain\yTrain;
predTrain = xTrain*theta;
predTest = xTest*theta;
errTrain = yTrain-predTrain;
errTest = yTest-predTest;

end