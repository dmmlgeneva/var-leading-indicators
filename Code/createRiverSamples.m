%%%%%%%%% MG 11/2/2017 - call functions to create river samples %%%%%%%

%% initiate path
%cd ..
%addpath(genpath('./functions'))
%% get the UGSG data
load('YellowAndConnect.mat','tsData')
%% create the UGSG raw datasets
numLags = 0; numLeading = 0; numLagsIn=5; 
tempData = tsData;
% for 20 replications
for repl = 1:20;
  tsData = tempData(1:end-repl+1,:);
  numTs = size(tsData,2); 
  expName = ['RV000_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
  fName = ['expData/',expName];
  mkdir(fName);
  save(fullfile(fName,'rawData.mat'),'tsData');
  prepInOut(fName,numLagsIn)
end



