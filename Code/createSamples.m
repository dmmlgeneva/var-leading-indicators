%%%%%%%%% MG 24/12/2016 - call functions to create samples %%%%%%%

%% initiate path
clear
[~,hn] = system('hostname')
if strcmp(hn(1:5),'magda') || strcmp(hn(1:5),'unige')
  cd /home/magda/Dropbox/Code/LeadingIndicators; % for testing on local machine
elseif strcmp(hn(1:4),'node')
  cd /home/gregorom/LeadingIndicators ; % on baobab
else
  cd /user/ai2/gregoma0/LeadingIndicators; % for testing on cluster
end
addpath(genpath('./functions'))
%% basic per-cluster setting
numTs = 10; numLags = 3; numLeading = 2;
%% generate var coefficients
numClusters = 10;
expCode = 'sp010';
%% getnerate the VAR coeffs
cTemp = [];
for idC = 1:numClusters
  while 1
		cCoeff = genVARCoeffs(numTs,numLags,numLeading);
    s = sum(sum(abs(cCoeff(1:numTs,1:numTs))));
    t = trace(abs(cCoeff(1:numTs,1:numTs)));
    if s>numLeading*t
      break
    else
      [s t]
    end
  end
  cTemp = [cTemp; cCoeff];
end
% split to cell by cluster and lag
aCell = mat2cell(cTemp,repmat(numTs,1,numClusters),repmat(numTs,1,numLags));
% construct the full coeff matrix
VARCoeffs = [];
for idLag = 1:numLags
	VARCoeffs = [VARCoeffs blkdiag(aCell{:,idLag})];
end
%% generate the ts data uisng the var coefficients
numLagsIn=5;
numTs = numTs * numClusters; numLeading = numLeading * numClusters; 
%VARCoeffs(1:numTs,1:numTs)
for repl = 1:20
  expName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
  genVARData(['expData/',expName],VARCoeffs,5000)
  % transform the ts data into input and output data structures
  prepInOut(['expData/',expName],numLagsIn)
end



