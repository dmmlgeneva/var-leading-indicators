function sumResults()
%%%%%%%%% MG 24/12/2016 - call functions for summarising results %%%%%%%

%% initiate path
clear
[~,hn] = system('hostname');
if strcmp(hn(1:5),'magda') || strcmp(hn(1:5),'unige')
  cd /home/magda/Dropbox/Code/LeadingIndicators; % for testing on local machine
elseif strcmp(hn(1:4),'node')
  cd /home/gregorom/LeadingIndicators ; % on baobab
else
  cd /user/ai2/gregoma0/LeadingIndicators; % for testing on cluster
end
if ~isdeployed
  addpath(genpath('./functions'))
end

%% disable warning
warning('off','MATLAB:load:variableNotFound')

%% set up experiment variables
expCode = 'sp001';
numTs = 10; numLags = 3; numLeading = 0; numLagsIn=5;
testLength=500; normalized = 1; numFolds = 3;
%% set up the job variations
trainLengthVec = [50 70 90 110 130 150 200 300 400 500];
%trainLengthVec = [50 70 90 110 130];
repls = 20;
% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

%% first summarise results at the cv level
for trainLength = trainLengthVec
  for repl = 1:repls;
    for cvI = 1:numFolds
      % set up folder names
      fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
      fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'_',num2str(cvI)]);
      sumMSE(fNameOut)
      sumTheta(fNameOut)
    end
  end
end

%% then summarise results at the outer train level (need the cv results for this)
for trainLength = trainLengthVec
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    sumMSE(fNameOut)
    sumTheta(fNameOut)
    sumCV(fNameOut)
  end
end

mkdir(['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)]);

%% and print all what's needed
printRMSE(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls);
printLbda(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls);
printTime(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls);
printIter(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls);
plotRegPath(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,3);
printRMSETests(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls);
plotTheta(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls);

end
