%%%%%%%%% MG 14/2/2017 - call functions to create StockWatson2012 samples %%%%%%%

%% initiate path
cd ..
addpath(genpath('./functions'))
%% get the SW2012 data
load('SW.mat','smallData')
%% create the SW raw datasets
numLags = 0; numLeading = 0; numLagsIn=5; 
% for 20 replications
for repl = 1:20;
  tsData = smallData(1:end-repl+1,:);
  numTs = size(tsData,2); 
  expName = ['SM000_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
  fName = ['expData/',expName];
  mkdir(fName);
  save(fullfile(fName,'rawData.mat'),'tsData');
  prepInOut(fName,numLagsIn)
end



