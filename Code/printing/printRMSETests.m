function printRMSETests(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls)
% PRINTRMSETESTS - print p-values of Wilcoxon's signrank and paired ttest on best RMSE across the experiment replications
%   
% INPUTS specifying the experiments
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   numLagsIn - number of lags to use for fitting the models
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
%   trainLengthVec - vector of train lengths
%   repls - replications of the experiment
% OUTPUTS
%   testRMSE.csv
%
% EXAMPLE:  printWilcox(numTs,numLags,numLeading,numLagsIn,testLength,normalized,numFolds,trainLengthVec,repls)
%
% CREATED: MG - 10/1/2017

%% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

%% get the results at the outer train level
wilcoxSCVAR = struct; wilcoxMCVAR = struct;
ttestSCVAR = struct; ttestMCVAR = struct;
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    load(fullfile(fNameOut,'sumCV.mat'),'bestMseFromCV');
    rmseTe(lIdx,repl) = structfun(@sqrt,bestMseFromCV,'UniformOutput',0);
  end
  %% calculate the average accros the experiment replications
  % loop through all methods
  metNames = fields(rmseTe);
  for mIdx = 1:numel(metNames)
    if isfield(rmseTe,'SCVAR')
      wilcoxSCVAR(lIdx).(metNames{mIdx}) = signrank( [rmseTe(lIdx,:).SCVAR] , [rmseTe(lIdx,:).(metNames{mIdx})],'tail','left');
      [~,ttestSCVAR(lIdx).(metNames{mIdx})] = ttest( [rmseTe(lIdx,:).SCVAR] , [rmseTe(lIdx,:).(metNames{mIdx})],'tail','left');
    end
    if isfield(rmseTe,'MCVAR')
      wilcoxMCVAR(lIdx).(metNames{mIdx}) = signrank( [rmseTe(lIdx,:).MCVAR], [rmseTe(lIdx,:).(metNames{mIdx})],'tail','left');
      [~,ttestMCVAR(lIdx).(metNames{mIdx})] = ttest( [rmseTe(lIdx,:).MCVAR], [rmseTe(lIdx,:).(metNames{mIdx})],'tail','left');
    end
  end
end

%% set up printing
pName = ['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)];
mkdir(pName)
pFile = fullfile(pName,'testRMSE.csv');
fileID = fopen(pFile,'w');
% print heading
fprintf(fileID,'p-values for Wilcoxon signrank test over %d replications for increasing training size \n%s \n%s \n \n',repls, pName, datestr(now,'dd/mm/yyyy HH:MM:SS'));

% setup printing formats
numMet = numel(metNames);
vecS = repmat([', %s'],1,numMet);
vecF = repmat([', %f'],1,numMet);
vecD = repmat([', %d'],1,numMet);

%% print the SCVAR results
if ~isempty(fieldnames(wilcoxSCVAR))
  % print table heading
  fprintf(fileID,'p-values for Wilcox for SCVAR over %d replications \n',repls);
  fprintf(fileID,['%s', vecS, '\n'],'Size/Methods',metNames{:});
  % print the p-values results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, '\n'], trainLength,struct2array(wilcoxSCVAR(lIdx)));
  end
end
%% print the MCVAR results
if ~isempty(fieldnames(wilcoxMCVAR))
  % print table heading
  fprintf(fileID,'\n \np-values for Wilcox for MCVAR over %d replications \n',repls);
  fprintf(fileID,['%s', vecS, '\n'],'Size/Methods',metNames{:});
  % print the p-values results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, '\n'], trainLength,struct2array(wilcoxMCVAR(lIdx)));
  end
end

% print heading
fprintf(fileID,'\n \n \np-values for paired ttest test over %d replications for increasing training size \n%s \n%s \n \n',repls, pName, datestr(now,'dd/mm/yyyy HH:MM:SS'));


%% print the SCVAR results
if ~isempty(fieldnames(ttestSCVAR))
  % print table heading
  fprintf(fileID,'p-values for ttest for SCVAR over %d replications \n',repls);
  fprintf(fileID,['%s', vecS, '\n'],'Size/Methods',metNames{:});
  % print the p-values results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, '\n'], trainLength,struct2array(ttestSCVAR(lIdx)));
  end
end

%% print the MCVAR results
if ~isempty(fieldnames(ttestMCVAR))
  % print table heading
  fprintf(fileID,'\n \np-values for ttest for MCVAR over %d replications \n',repls);
  fprintf(fileID,['%s', vecS, '\n'],'Size/Methods',metNames{:});
  % print the p-values results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, '\n'], trainLength,struct2array(ttestMCVAR(lIdx)));
  end
end


%% close the printfile  
fclose(fileID);

end

