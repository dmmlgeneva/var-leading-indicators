function printTime(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls)
% PRINTTIME - print train time for best hyper-param from cross-validation for all methods and train sizes
%   
% INPUTS specifying the experiments
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   numLagsIn - number of lags to use for fitting the models
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
%   trainLengthVec - vector of train lengths
%   repls - replications of the experiment
% OUTPUTS
%   runTimeAvg.csv
%
% EXAMPLE:  printTime(numTs,numLags,numLeading,numLagsIn,testLength,normalized,numFolds,trainLengthVec,repls)
%
% CREATED: MG - 8/1/2017

%% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

%% get the results at the outer train level
avgRunTime = struct;
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    load(fullfile(fNameOut,'sumCV.mat'),'bestCVLbdaTime');
    runTime(lIdx,repl) = bestCVLbdaTime;
  end
  %% calculate the average accros the experiment replications
  % loop through all methods
  metNames = fields(runTime);
  for mIdx = 1:numel(metNames)
    avgRunTime(lIdx).(metNames{mIdx}) = mean([runTime(lIdx,:).(metNames{mIdx})]);
  end
end

%% set up printing
pName = ['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)];
mkdir(pName)
pFile = fullfile(pName,'runTimeAvg.csv');
fileID = fopen(pFile,'w');
% print heading
fprintf(fileID,'Average runTime over %d replications for increasing training size \n%s \n%s \n \n',repls, pName, datestr(now,'dd/mm/yyyy HH:MM:SS'));

% setup printing formats
numMet = numel(metNames);
vecS = repmat([', %s'],1,numMet);
vecF = repmat([', %f'],1,numMet);
vecD = repmat([', %d'],1,numMet);

%% first print the averages accross the replications
% print table heading
fprintf(fileID,'Average runTime over %d replications \n',repls);
fprintf(fileID,['%s', vecS, '\n'],'Size/Methods',metNames{:});
% print rmse results for all train sizes
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  fprintf(fileID,['%d', vecF, '\n'], trainLength,struct2array(avgRunTime(lIdx)));
end


%% then print each individual replication
% print table heading
fprintf(fileID,'\n \nrunTime for each replication \n',repls);
for repl = 1:repls
  fprintf(fileID,'\nReplication, %d \n',repl);
  fprintf(fileID,['%s', vecS, '\n'],'Size/Methods',metNames{:});
  % print rmse results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, '\n'], trainLength,struct2array(runTime(lIdx,repl)));
  end
end


%% close the printfile  
fclose(fileID);

end
