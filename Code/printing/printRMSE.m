function printRMSE(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls)
% PRINTRMSE - print RMSE for best hyper-param from cross-validation for all methods and train sizes
%   
% INPUTS specifying the experiments
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   numLagsIn - number of lags to use for fitting the models
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
%   trainLengthVec - vector of train lengths
%   repls - replications of the experiment
% OUTPUTS
%   rmseTestAvg.csv
%
% EXAMPLE:  printRMSE(numTs,numLags,numLeading,numLagsIn,testLength,normalized,numFolds,trainLengthVec,repls)
%
% CREATED: MG - 8/1/2017

%% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

%% get the results at the outer train level
avgRmseTest = struct; avgRelMseTest = struct;
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    load(fullfile(fNameOut,'sumCV.mat'),'bestMseFromCV');
    rmseTe(lIdx,repl) = structfun(@sqrt,bestMseFromCV,'UniformOutput',0);
    %% load the baseMod errors for calculating relative MSE
    if isfield(bestMseFromCV,'TRUE')
      baseMse = bestMseFromCV.TRUE;
      baseModName = 'TRUE';
    else  
      baseMse = bestMseFromCV.RW;
      baseModName = 'RW';
    end
    relMseTe(lIdx,repl) = structfun(@(x) x/baseMse,bestMseFromCV,'UniformOutput',0);
  end
  %% calculate the average accros the experiment replications
  % loop through all methods
  metNames = fields(rmseTe);
  for mIdx = 1:numel(metNames)
    avgRmseTest(lIdx).(metNames{mIdx}) = mean([rmseTe(lIdx,:).(metNames{mIdx})]);
    avgRelMseTest(lIdx).(metNames{mIdx}) = mean([relMseTe(lIdx,:).(metNames{mIdx})]);
  end
end


%% save the summary MSE results
pName = ['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)];
mkdir(pName)
save(['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'/mse.mat'],'rmseTe','relMseTe','avgRmseTest','avgRelMseTest')

%% set up printing
pFile = fullfile(pName,'rmseTestAvg.csv');
fileID = fopen(pFile,'w');
% print heading
fprintf(fileID,'Average RMSETest and relative MSE over %d replications for increasing training size \nover %s as base \n%s \n%s \n \n',repls, baseModName, pName, datestr(now,'dd/mm/yyyy HH:MM:SS'));

% setup printing formats
numMet = numel(metNames);
vecS = repmat([', %s'],1,numMet);
vecF = repmat([', %f'],1,numMet);
vecD = repmat([', %d'],1,numMet);

%% first print the averages accross the replications
% print table heading
fprintf(fileID,'Average RMSETest and relMSE over %d replications \n',repls);
fprintf(fileID,['%s', vecS, ', , %s', vecS, '\n'],'Size/Methods',metNames{:},'Size/Methods',metNames{:});
% print rmse results for all train sizes
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  fprintf(fileID,['%d', vecF, ', , %d', vecF, '\n'], trainLength,struct2array(avgRmseTest(lIdx)),trainLength,struct2array(avgRelMseTest(lIdx)));
end


%% then print each individual replication
% print table heading
fprintf(fileID,'\n \nRMSETest and relMSE for each replication \n',repls);
for repl = 1:repls
  fprintf(fileID,'\nReplication, %d \n',repl);
  fprintf(fileID,['%s', vecS, ', , %s', vecS, '\n'],'Size/Methods',metNames{:},'Size/Methods',metNames{:});
  % print rmse results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, ', , %d', vecF, '\n'], trainLength,struct2array(rmseTe(lIdx,repl)),trainLength,struct2array(relMseTe(lIdx,repl)));
  end
end


%% close the printfile  
fclose(fileID);

end

