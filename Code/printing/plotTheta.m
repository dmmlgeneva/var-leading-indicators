function plotTheta(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls)
% PLOTTHETA - plot learned theta for best hyper-param from cross-validation for all methods and train sizes
%   
% INPUTS specifying the experiments
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   numLagsIn - number of lags to use for fitting the models
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
%   trainLengthVec - vector of train lengths
%   repls - replications of the experiment
% OUTPUTS
%   rmseTestAvg.csv
%
% EXAMPLE:  printTheta(numTs,numLags,numLeading,numLagsIn,testLength,normalized,numFolds,trainLengthVec,repls)
%
% CREATED: MG - 8/1/2017

%% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

% for recoding to zero-onex
function x=grMaj(x)
  x(x<=0.5) = 0;
  x(x>0.5) = 1;
end

%% get the results at the outer train level
learnGranger = struct; 
learnGrangerAvg = struct; 
bestTheta = struct;
avgFalseNegativeRate = struct; avgFalsePositiveRate = struct; avgFNRFPR = struct;
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  
  %% get the learned granger structures across all methods and replications
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    % load the learned thetas
    load(fullfile(fNameOut,'sumCV.mat'),'bestThetaFromCV');
    bestThetaFromCV=rmfield(bestThetaFromCV,'MEAN'); % this does not have the structure as all the other ones
    metNames = fields(bestThetaFromCV);
    for mIdx = 1:numel(metNames)
      lCell = mat2cell(bestThetaFromCV.(metNames{mIdx}),repmat(numTs,1,numLagsIn),numTs);
      lG = sum(abs(cat(3,lCell{:})),3);
      lG(lG~=0) = 1;
      learnGranger(lIdx,repl).(metNames{mIdx}) = lG;
      bestTheta(lIdx,repl).(metNames{mIdx}) =  bestThetaFromCV.(metNames{mIdx}); % to be saved as mat
      if length(learnGrangerAvg)>=lIdx && isfield(learnGrangerAvg(lIdx),metNames{mIdx}) && ~isempty(learnGrangerAvg(lIdx).(metNames{mIdx}))
        learnGrangerAvg(lIdx).(metNames{mIdx}) = learnGrangerAvg(lIdx).(metNames{mIdx}) + learnGranger(lIdx,repl).(metNames{mIdx})/repls;
      else
        learnGrangerAvg(lIdx).(metNames{mIdx}) = learnGranger(lIdx,repl).(metNames{mIdx})/repls;
      end
    end
  end
  % recode the averages to zero ones
  learnGrangerAvgForPlot(lIdx) = learnGrangerAvg(lIdx);
  learnGrangerAvg(lIdx) = structfun(@(x) grMaj(x), learnGrangerAvg(lIdx),'UniformOutput',0);
  
  %% calculate the discrepencies from the average
  for repl = 1:repls
    for mIdx = 1:numel(metNames)
      % fraction of relevant variables that were not selected
      numRel = sum(learnGrangerAvg(lIdx).(metNames{mIdx})(:));
      numIrrel = prod(size(learnGrangerAvg(lIdx).(metNames{mIdx})))-numRel;
      falseNegativeRate(lIdx,repl).(metNames{mIdx}) = length(find(learnGrangerAvg(lIdx).(metNames{mIdx})-learnGranger(lIdx,repl).(metNames{mIdx}) == 1))/numRel;
      if length(avgFalseNegativeRate)>=lIdx && isfield(avgFalseNegativeRate(lIdx),metNames{mIdx}) && ~isempty(avgFalseNegativeRate(lIdx).(metNames{mIdx}))
        avgFalseNegativeRate(lIdx).(metNames{mIdx}) = avgFalseNegativeRate(lIdx).(metNames{mIdx}) + falseNegativeRate(lIdx,repl).(metNames{mIdx})/repls;
      else
        avgFalseNegativeRate(lIdx).(metNames{mIdx}) = falseNegativeRate(lIdx,repl).(metNames{mIdx})/repls;
      end
      % fraction of irrrelevant variables that were selected
      falsePositiveRate(lIdx,repl).(metNames{mIdx}) = length(find(learnGrangerAvg(lIdx).(metNames{mIdx})-learnGranger(lIdx,repl).(metNames{mIdx}) == -1))/numIrrel;
      if length(avgFalsePositiveRate)>=lIdx && isfield(avgFalsePositiveRate(lIdx),metNames{mIdx}) && ~isempty(avgFalsePositiveRate(lIdx).(metNames{mIdx}))
        avgFalsePositiveRate(lIdx).(metNames{mIdx}) = avgFalsePositiveRate(lIdx).(metNames{mIdx}) + falsePositiveRate(lIdx,repl).(metNames{mIdx})/repls;
      else
        avgFalsePositiveRate(lIdx).(metNames{mIdx}) = falsePositiveRate(lIdx,repl).(metNames{mIdx})/repls;
      end
      % and the average of the two
      FNRFPR(lIdx,repl).(metNames{mIdx}) = (falseNegativeRate(lIdx,repl).(metNames{mIdx})+falsePositiveRate(lIdx,repl).(metNames{mIdx}))/2;
      if length(avgFNRFPR)>=lIdx && isfield(avgFNRFPR(lIdx),metNames{mIdx}) && ~isempty(avgFNRFPR(lIdx).(metNames{mIdx}))
        avgFNRFPR(lIdx).(metNames{mIdx}) = avgFNRFPR(lIdx).(metNames{mIdx}) + FNRFPR(lIdx,repl).(metNames{mIdx})/repls;
      else
        avgFNRFPR(lIdx).(metNames{mIdx}) = FNRFPR(lIdx,repl).(metNames{mIdx})/repls;
      end
    end
  end
  
end

%% save the summary MSE results
save(['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'/theta.mat'],'bestTheta')


%% set up printing
pName = ['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)];
mkdir(pName)
pFile = fullfile(pName,'thetaStability.csv');
fileID = fopen(pFile,'w');
% print heading
fprintf(fileID,'Average FPR FNR and the mean of the two over compared to the average of Granger over %d replications for increasing training size \n%s \n%s \n \n',repls, pName, datestr(now,'dd/mm/yyyy HH:MM:SS'));

% setup printing formats
numMet = numel(metNames);
vecS = repmat([', %s'],1,numMet);
vecF = repmat([', %f'],1,numMet);
vecD = repmat([', %d'],1,numMet);

%% first print the averages accross the replications
% print table heading
fprintf(fileID,'Average FPR FNR and the mean of the two over %d replications \n',repls);
fprintf(fileID,['%s', vecS, ', , %s', vecS,  ', , %s', vecS, '\n'],'Size/FPR',metNames{:},'Size/FNR',metNames{:},'Size/avgFNRFPR',metNames{:});
% print mse results for all train sizes
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  fprintf(fileID,['%d', vecF, ', , %d', vecF,  ', , %d', vecF, '\n'], trainLength,struct2array(avgFalsePositiveRate(lIdx)), trainLength, struct2array(avgFalseNegativeRate(lIdx)), trainLength, struct2array(avgFNRFPR(lIdx)));
end


%% then print each individual replication
% print table heading
fprintf(fileID,'\n \nFPR FNR and the mean of the two for each replication \n',repls);
for repl = 1:repls
  fprintf(fileID,'\nReplication, %d \n',repl);
  fprintf(fileID,['%s', vecS, ', , %s', vecS, ', , %s', vecS, '\n'],'Size/FPR',metNames{:},'Size/FNR',metNames{:},'Size/avgFNRFPR',metNames{:});
  % print mse results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, ', , %d', vecF,  ', , %d', vecF, '\n'], trainLength,struct2array(falsePositiveRate(lIdx,repl)), trainLength,struct2array(falseNegativeRate(lIdx,repl)), trainLength,struct2array(FNRFPR(lIdx,repl)));
  end
end


%% close the printfile  
fclose(fileID);

%% load the true coefficients (do this just for the 1st (do this just in the 1st replication, the other ones are the same)
load(fullfile('expData',fName,'rawData.mat'),'VARCoeffs');
if exist('VARCoeffs','var')
  % reformat the coeffs so that they have the same structure as thetas
  tCell = mat2cell(VARCoeffs,numTs,repmat(numTs,1,numLags));
  trueGranger = sum(abs(cat(3,tCell{:})),3)';
  trueGranger(trueGranger~=0) = 1;
  tGExist=1;
else
  tGExist=0;
end

%% do the plotting
listMethods = {'TRUE','VARL1','VARLG','SCVAR','MCVAR'};
for lIdx = 1:length(trainLengthVec)
  % add true granger into the list of grangers to plot
  if tGExist
    learnGrangerAvgForPlot(lIdx).TRUE = trueGranger;
  end
  % make list of methods just based on those really produced
  metNames = listMethods(cellfun(@(x) isfield(learnGrangerAvgForPlot(lIdx),x), listMethods));
  numMethods = length(metNames);
  % plot the subfigures
  hm(lIdx) = figure('Name',['trainLength = ',num2str(trainLengthVec(lIdx))],'NumberTitle','off');
  myMap = flipud(colormap(gray));
  colormap(myMap)
  for mIdx = 1:numMethods
    subplot(1,numMethods,mIdx)
    imagesc(learnGrangerAvgForPlot(lIdx).(metNames{mIdx}))
    title(metNames(mIdx))
  end 
end
% store all as a single matlab figure
pFile = fullfile(pName,'grangerPlots.fig');
savefig(hm,pFile)
close(hm);


end

