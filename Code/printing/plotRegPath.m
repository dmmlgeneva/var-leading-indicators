function plotRegPath(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls)
% PLOTREGPATH - plot regularization path and runtime by hyper-parameter combinations
%   
% INPUTS specifying the experiments
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   numLagsIn - number of lags to use for fitting the models
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
%   trainLengthVec - vector of train lengths
%   repls - replications of the experiment
% OUTPUTS
%   regPath.fig and timePath.fig in expPrints/[expName] folder
%
% EXAMPLE:  plotRegPath(numTs,numLags,numLeading,numLagsIn,testLength,normalized,numFolds,trainLengthVec,repls)
%
% CREATED: MG - 8/1/2017

%% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

%% plotting function
function plotRP(tp,plotData,legendText,rep,bLbdaIdx,bLbda,bMse)
  plot(plotData)
  legend(legendText{:})
  ylabel(tp);
  xlabel('lbdIdx');
  title(sprintf('rep=%d',rep));
  % and annotate the best cv lbda
  txt1 = ['\leftarrow best',tp,'=',num2str(bMse,'%8.5f')];
  txt2 = ['\leftarrow for lbda=',mat2str(bLbda)];
  text(bLbdaIdx,bMse,txt1)
  text(bLbdaIdx,0,txt2)
  hold on;
  plot([bLbdaIdx bLbdaIdx],[0 bMse],'-r')
  hold off;
end

%% get the regularization path and runTime for the hyper-parameter combinations
fMSE = 0; fTime =0; fIter =0;
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  %% first gather the results accross the replications
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    % get the mse and time results
    load(fullfile(fNameOut,'sumMSE.mat'),'mseTest','grids','runTime','iters');
    % get the cross-validated hyper-parmas (and corresponding mse and time)
    load(fullfile(fNameOut,'sumCV.mat'),'bestCVLbda','bestMseFromCV','bestCVLbdaTime','bestCVIters');
    metList = fields(grids);
    numMet = numel(metList);
    % process each method seperately
    for metIdx = 1:numMet;
      tmpGr = grids.(metList{metIdx}); % get the grid combination matrix
      bLbd(repl).(metList{metIdx}) = tmpGr(bestCVLbda.(metList{metIdx}),:); % get the best combination
      bLbdIdx = find(bLbd(repl).(metList{metIdx})(:,1)==unique(tmpGr(:,1))); % find the index of the best combination in the grid
      bLbd(repl).(metList{metIdx})(:,1)=bLbdIdx; % replace the lbd value by its index (better for plotting)
      bMse(repl).(metList{metIdx}) = bestMseFromCV.(metList{metIdx}); % get the corresponding best mse
      bTime(repl).(metList{metIdx}) = bestCVLbdaTime.(metList{metIdx}); % get the corresponding best runTime
      if isfield(bestCVIters,(metList{metIdx}))
        bIters(repl).(metList{metIdx}) = bestCVIters.(metList{metIdx}); % get the corresponding best iter num
      end
      if size(tmpGr,2) > 1 % need to process differently if multiple hyper-parameters
        % regroup results so that each group has all lbdas for the other combinations of hyper-parameters
        combGr = tmpGr; combGr(:,1) = [];
        [unGr,ida,idc] = unique(combGr,'rows');
        pDataTemp = zeros(length(unique(tmpGr(:,1))),size(unGr,1));
        tDataTemp = zeros(length(unique(tmpGr(:,1))),size(unGr,1));
        iDataTemp = zeros(length(unique(tmpGr(:,1))),size(unGr,1));
        legTextTemp = cell(size(unGr,1),1);
        for gIdx = unique(idc)'
          pDataTemp(:,gIdx) = mseTest.(metList{metIdx})(gIdx==idc);
          tDataTemp(:,gIdx) = runTime.(metList{metIdx})(gIdx==idc);
          iDataTemp(:,gIdx) = iters.(metList{metIdx})(gIdx==idc);
          legTextTemp{gIdx} = mat2str(unGr(gIdx,:));
        end
      else % or just a single hyper-parameter
        pDataTemp = mseTest.(metList{metIdx});
        tDataTemp = runTime.(metList{metIdx});
        if isfield(iters,(metList{metIdx}))
          iDataTemp = iters.(metList{metIdx});
        end
        legTextTemp{1} = 'single';
      end
      % stock the plot data by method
      pData(repl).(metList{metIdx}) = pDataTemp;
      tData(repl).(metList{metIdx}) = tDataTemp;
      if exist('iDataTemp','var')
        iData(repl).(metList{metIdx}) = iDataTemp;
      end
      legText(repl).(metList{metIdx}) = legTextTemp;
      clear legTextTemp pDataTemp tDataTemp iDataTemp;
      fprintf('Processed trainLength=%d replication=%d for %s\n',trainLength,repl,metList{metIdx})
    end
  end
  %% here need to do the plotting!!!!
  % the mse regularization path
  metList = fields(pData);
  numMet = numel(metList);
  for metIdx = 1:numMet
    for repl = 1:repls
      fMSE = fMSE+1;
      figMSE(fMSE) = figure('Name',[(metList{metIdx}), ' ',num2str(trainLength), ' ',num2str(repl)],'NumberTitle','off','Visible','off','Tag',num2str(trainLength));
      plotRP('MSE',pData(repl).(metList{metIdx}),legText(repl).(metList{metIdx}),repl,bLbd(repl).(metList{metIdx})(:,1),bLbd(repl).(metList{metIdx}),bMse(repl).(metList{metIdx}))
    end
  end
  % the the runTime by hyper-param combination
  metList = fields(tData);
  numMet = numel(metList);
  for metIdx = 1:numMet
    for repl = 1:repls
      fTime = fTime+1;
      figTime(fTime) = figure('Name',[(metList{metIdx}), ' ',num2str(trainLength), ' ',num2str(repl)],'NumberTitle','off','Visible','off','Tag',num2str(trainLength));
      plotRP('time',tData(repl).(metList{metIdx}),legText(repl).(metList{metIdx}),repl,bLbd(repl).(metList{metIdx})(:,1),bLbd(repl).(metList{metIdx}),bTime(repl).(metList{metIdx}))
    end
  end
  % the the iterations by hyper-param combination
  metList = fields(iData);
  numMet = numel(metList);
  for metIdx = 1:numMet
    for repl = 1:repls
      fIter = fIter+1;
      figIter(fIter) = figure('Name',[(metList{metIdx}), ' ',num2str(trainLength), ' ',num2str(repl)],'NumberTitle','off','Visible','off','Tag',num2str(trainLength));
      plotRP('iters',iData(repl).(metList{metIdx}),legText(repl).(metList{metIdx}),repl,bLbd(repl).(metList{metIdx})(:,1),bLbd(repl).(metList{metIdx}),bIters(repl).(metList{metIdx}))
    end
  end
end
% store all the plots as single matlab figure
pName = ['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)];
mkdir(pName)
pFile = fullfile(pName,'regPath.fig');
savefig(figMSE,pFile)
close(figMSE);
% 2nd matlab figures
tFile = fullfile(pName,'timePath.fig');
savefig(figTime,tFile)
close(figTime)
% 3d matlab figures
iFile = fullfile(pName,'iterPath.fig');
savefig(figIter,iFile)
close(figIter)

end



