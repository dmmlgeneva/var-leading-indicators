function printLbda(expCode,numTs,numLags,numLeading,numLagsIn,testLength,normalized,trainLengthVec,repls)
% PRINTLBDA - print best hyper-param from cross-validation for all methods and train sizes
%   
% INPUTS specifying the experiments
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   numLagsIn - number of lags to use for fitting the models
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
%   trainLengthVec - vector of train lengths
%   repls - replications of the experiment
% OUTPUTS
%   bestLbda.csv
%
% EXAMPLE:  printIter(numTs,numLags,numLeading,numLagsIn,testLength,normalized,numFolds,trainLengthVec,repls)
%
% CREATED: MG - 8/1/2017

%% get the correct file names for normalized(or not) data
if normalized
  fileName = 'ttNorm';
else
  fileName = 'tt';
end

%% get the results at the outer train level
avgCVLbda = struct; avgLbda = struct; stdCVLbda = struct; stdLbda = struct;
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  for repl = 1:repls
    % set up folder names
    fName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
    fNameOut = fullfile('expResults/',fName,[fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)]);
    load(fullfile(fNameOut,'sumCV.mat'),'bestCVLbda','bestLbda');
    CVLbda(lIdx,repl) = bestCVLbda;
    Lbda(lIdx,repl) = bestLbda;
  end
  %% calculate the average accros the experiment replications
  % loop through all methods
  metNames = fields(CVLbda);
  for mIdx = 1:numel(metNames)
    avgCVLbda(lIdx).(metNames{mIdx}) = mean([CVLbda(lIdx,:).(metNames{mIdx})]);
    avgLbda(lIdx).(metNames{mIdx}) = mean([Lbda(lIdx,:).(metNames{mIdx})]);
    stdCVLbda(lIdx).(metNames{mIdx}) = std([CVLbda(lIdx,:).(metNames{mIdx})]);
    stdLbda(lIdx).(metNames{mIdx}) = std([Lbda(lIdx,:).(metNames{mIdx})]);
  end
end
%% get the grids from the last dataset (they should be the same everywhere)
load(fullfile(fNameOut,'sumMSE.mat'),'grids');

%% set up printing
pName = ['expPrints/',expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading)];
mkdir(pName)
pFile = fullfile(pName,'bestLbda.csv');
fileID = fopen(pFile,'w');
% print heading
fprintf(fileID,'Best lambda indexes from cv and from test over %d replications for increasing training size \n%s \n%s \n \n',repls, pName, datestr(now,'dd/mm/yyyy HH:MM:SS'));

% setup printing formats
numMet = numel(metNames);
vecS = repmat([', %s'],1,numMet);
vecF = repmat([', %f'],1,numMet);
vecD = repmat([', %d'],1,numMet);

%% first print the averages accross the replications
% print table heading
fprintf(fileID,'Average and standard deviation of lambda indexes over %d replications \n',repls);
fprintf(fileID,['%s', vecS, ', , %s', vecS,  ', , , %s', vecS,   ', , %s', vecS, '\n'],'Size/AvgCVLbda',metNames{:},'Size/AvgLbda',metNames{:},'Size/StdCVLbda',metNames{:},'Size/StdLbda',metNames{:});
% print rmse results for all train sizes
for lIdx = 1:length(trainLengthVec)
  trainLength = trainLengthVec(lIdx);
  fprintf(fileID,['%d', vecF, ', , %d', vecF,  ', , , %d', vecF, ', , %d', vecF, '\n'], trainLength,struct2array(avgCVLbda(lIdx)), trainLength, struct2array(avgLbda(lIdx)), trainLength, struct2array(stdCVLbda(lIdx)), trainLength, struct2array(stdLbda(lIdx)));
end


%% then print each individual replication
% clean Lbda from extra method fields
f=fields(Lbda); toRemove = f(~ismember(f,metNames));
Lbda = rmfield(Lbda,[toRemove]);
% print table heading
fprintf(fileID,'\n \nBest lambda indexes for each replication \n',repls);
for repl = 1:repls
  fprintf(fileID,'\nReplication, %d \n',repl);
  fprintf(fileID,['%s', vecS, ', , %s', vecS, '\n'],'Size/CVLbda',metNames{:},'Size/Lbda',metNames{:});
  % print rmse results for all train sizes
  for lIdx = 1:length(trainLengthVec)
    trainLength = trainLengthVec(lIdx);
    fprintf(fileID,['%d', vecF, ', , %d', vecF, '\n'], trainLength,struct2array(CVLbda(lIdx,repl)), trainLength,struct2array(Lbda(lIdx,repl)));
  end
end

%% finally print the grids
metNames=fields(grids);
numGr = structfun(@(x) size(x,2),grids);
% replicate names
longNames = cell2mat(cellfun(@(x,y) repmat(x,y,1),metNames,num2cell(numGr),'UniformOutput',0));
metNames = mat2cell(longNames, repmat(1,1,size(longNames,1)),size(longNames,2));
% make into 1 large matrix
maxSize = max(structfun(@(x) size(x,1),grids));
newGrid = struct2cell(structfun(@(x) padarray(x,[(maxSize-size(x,1)) 0],'post'),grids,'UniformOutput',0));
newG = horzcat(newGrid{:});
% setup printing formats
vecS = repmat([', %s'],1,numel(metNames));
vecF = repmat([', %f'],1,size(newG,2));
% print table heading
fprintf(fileID,'\n \nGrids \n',repls);
fprintf(fileID,['%s', vecS, '\n'],'Methods',metNames{:});
for lnIdx = 1:size(newG,1)
  fprintf(fileID,['%d', vecF, '\n'], lnIdx,newG(lnIdx,:));
end



%% close the printfile  
fclose(fileID);

end
