function predSCVAR(expCode,numTs,numLags,numLeading,repl,numLagsIn,trainLength,testLength,normalized,numFold,lbdaGrid,lbdaGridActive,kappaGrid,kappaGridActive) 
% PREDSCVAR - fit SCVAR model
%   
% INPUTS specifying the correct experiment
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   repl - replication of the experiment
% INPUTS specifying the data set to fit
%   numLagsIn - number of lags to use for fitting the models
%   trainLength - length of the training sample
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFold - number of the inner cross validation fold (= 0 if the outer train-test)
% INPUTS optional
%   lbdaGrid, kappaGrid - grid for the lambda and kappa search
%   lbdaGridActive,kappaGridActive - 0/1 vector indicating the active part of the lbdaGrid for search (the rest of the grid is there just for compatibility)
% OUTPUTS
% 
% EXAMPLE:  = predSCVAR('synth',5,3,1,1,5,30,500,1,5) 
%
% CREATED: MG - 1/1/2017


%% correct var types if deployed
if isdeployed
  numTs = str2num(numTs); numLags = str2num(numLags); numLeading = str2num(numLeading);
  repl = str2num(repl); numLagsIn = str2num(numLagsIn); trainLength = str2num(trainLength); 
  testLength = str2num(testLength); normalized = str2num(normalized); numFold = str2num(numFold); 
  [~,hn] = system('hostname');
  if strcmp(hn(1:4),'node')
    cd /home/gregorom/LeadingIndicators ; % on baobab
  else
    cd /user/ai2/gregoma0/LeadingIndicators; % for testing on cluster
  end
  eval(['lbdaGridActive = ' lbdaGridActive ';']);
  eval(['lbdaGrid = ' lbdaGrid ';']);
  eval(['kappaGridActive = ' kappaGridActive ';']);
  eval(['kappaGrid = ' kappaGrid ';']);
else
  [~,hn] = system('hostname');
  if strcmp(hn(1:5),'magda') || strcmp(hn(1:5),'unige')
    cd /home/magda/Dropbox/Code/LeadingIndicators; % for testing on local machine
  else
    cd /user/ai2/gregoma0/LeadingIndicators; % for testing on cluster
  end
end

tic;
%% fill in optional arguments
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = logspace(-4,3,15);
end
if ~exist('kappaGrid','var') || isempty(kappaGrid),
  %kappaGrid = logspace(log10(1/numTs),log10(numTs),3);
  kappaGrid = [0.5 1 2];
end
if ~exist('lbdaGridActive','var') || isempty(lbdaGridActive),
  lbdaGridActive = ones(length(lbdaGrid));
end
if ~exist('kappaGridActive','var') || isempty(kappaGridActive),
  kappaGridActive = ones(length(kappaGrid));
end

%% fix folder and file names
% experiment name
expName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
% get the correct file names for normalized(or not) data
if normalized fileName = 'ttNorm'; else fileName = 'tt'; end

if numFold == 0 % for the outer train-test samples
  %% fit base models to train data
  % datasample name
  dsName = [fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)];
  fprintf('BEGIN predSCVAR %s %s \n',expName,dsName)
  % output folder
  fNameOut = fullfile('expResults/',expName,dsName);
  % load VARL2 results
  load(fullfile(fNameOut,'sumTheta.mat'),'Thetas')
  thetaVARL2 = Thetas.VARL2;
  % load ts data
  load(fullfile('expData/',expName,[dsName,'.mat']),'yTrain','xTrain','yTest','xTest')
  % fit and save the results
  [predTrain,predTest,errTrain,errTest,theta,time,numIter,objHistory,numIterSimplexAvg,timeSimplexAvg] = fitSCVAR(yTrain,xTrain,yTest,xTest,lbdaGrid,lbdaGridActive,kappaGrid,kappaGridActive,thetaVARL2);
  save(fullfile(fNameOut,'SCVAR.mat'),'predTrain','predTest','errTrain','errTest','theta','lbdaGrid','lbdaGridActive','kappaGrid','kappaGridActive','time','numIter','objHistory','numIterSimplexAvg','timeSimplexAvg')
  elapsedTime = toc;
  fprintf('END predSCVAR %s %s time: %6.2f \n',expName,dsName,elapsedTime)
else % for the inner cross-validation train test
   % datasample name
  dsName = [fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'_',num2str(numFold)];
  fprintf('BEGIN predSCVAR %s %s cv=%d \n',expName,dsName,numFold)
  % output folder
  fNameOut = fullfile('expResults/',expName,dsName);
  % load VARL2 results
  load(fullfile(fNameOut,'sumTheta.mat'),'Thetas')
  thetaVARL2 = Thetas.VARL2;
  % load ts data
  load(fullfile('expData/',expName,[dsName,'.mat']),'yTrain','xTrain','yTest','xTest')
  % fit and save the results
  [predTrain,predTest,errTrain,errTest,theta,time,numIter,objHistory,numIterSimplexAvg,timeSimplexAvg] = fitSCVAR(yTrain,xTrain,yTest,xTest,lbdaGrid,lbdaGridActive,kappaGrid,kappaGridActive,thetaVARL2);
  save(fullfile(fNameOut,'SCVAR.mat'),'predTrain','predTest','errTrain','errTest','theta','time','numIter','objHistory','lbdaGrid','lbdaGridActive','kappaGrid','kappaGridActive','numIterSimplexAvg','timeSimplexAvg')
  elapsedTime = toc;
  fprintf('END predSCVAR %s %s cv=%d time: %6.2f \n',expName,dsName,numFold,elapsedTime)
end


end
