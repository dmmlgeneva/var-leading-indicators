function predBaseModels(expCode,numTs,numLags,numLeading,repl,numLagsIn,trainLength,testLength,normalized,numFolds,lbdaGrid) 
% PREDBASEMODELS - preprocess exp data into modelling structures, 
%   fit all base models (including VARL2 in the cv samples) and summarise results
%   
% INPUTS specifying the correct experiment
%   expCode - epxeriment name
%   numTs - number of ts
%   numLags - number of AR lags of the generating process
%   numLeading - number of leading indicators in the generating process
%   repl - replication of the experiment
% INPUTS specifying the data set to fit
%   numLagsIn - number of lags to use for fitting the models
%   trainLength - length of the training sample
%   testLength - lenghth of the test sample
%   normalized - work with normalized data=1 (or not = 0)
%   numFolds - number of folds for the inner cross validation
% INPUTS specifying the fit run
%   lbdaGrid - grid for the lambda search
% OUTPUTS
% 
% EXAMPLE:  = predBaseModels('synth',5,3,1,1,1,5,30,500,5) 
%
% CREATED: MG - 1/1/2017

tic;
%% fill in optional arguments
if ~exist('lbdaGrid','var') || isempty(lbdaGrid),
  lbdaGrid = logspace(-4,3,15);
end

%% preprocess raw experiment data into modelling structures
% experiment name
expName = [expCode,'_',num2str(numTs),'_',num2str(numLags),'_',num2str(numLeading),'_',num2str(repl)];
fprintf('BEGIN predBaseModels %s trainLength=%d \n',expName,trainLength)
% generate train and test samples
sp = 1; % is single(=1) or double precision
prepTrainTest(['expData/',expName],numLagsIn,trainLength,testLength,normalized,numFolds,sp)

%% fit base models to train data
% get the correct file names for normalized(or not) data
if normalized fileName = 'ttNorm'; else fileName = 'tt'; end
% datasample name
dsName = [fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)];
% load data
load(fullfile('expData/',expName,[dsName,'.mat']),'yTrain','xTrain','yTest','xTest')
% prepare output folder
fNameOut = fullfile('expResults/',expName,dsName);
mkdir(fNameOut)
% fit and save the simple models
[predTrain,predTest,errTrain,errTest,theta] = fitAR(yTrain,xTrain,yTest,xTest);
save(fullfile(fNameOut,'AR.mat'),'predTrain','predTest','errTrain','errTest','theta')
[predTrain,predTest,errTrain,errTest,theta] = fitVAR(yTrain,xTrain,yTest,xTest);
save(fullfile(fNameOut,'VAR.mat'),'predTrain','predTest','errTrain','errTest','theta')
[predTrain,predTest,errTrain,errTest,theta] = fitMEAN(yTrain,xTrain,yTest,xTest);
save(fullfile(fNameOut,'MEAN.mat'),'predTrain','predTest','errTrain','errTest','theta')
[predTrain,predTest,errTrain,errTest,theta] = fitRW(yTrain,xTrain,yTest,xTest);
save(fullfile(fNameOut,'RW.mat'),'predTrain','predTest','errTrain','errTest','theta')
[predTrain,predTest,errTrain,errTest,theta,time] = fitVARL2(yTrain,xTrain,yTest,xTest,lbdaGrid);
save(fullfile(fNameOut,'VARL2.mat'),'predTrain','predTest','errTrain','errTest','theta','lbdaGrid','time')
% do TRUE model if we know the true coefficients
varInfo = who('-file', fullfile('expData/',expName,'rawData.mat'));
if ismember('VARCoeffs', varInfo) % returns true
  load(fullfile('expData/',expName,'rawData.mat'),'VARCoeffs'); % load 'VARCoeffs'
  if normalized % if normalized load yMean and yStd
    load(fullfile('expData/',expName,[dsName,'.mat']),'yMean','yStd') 
  else % else make them standard
    yMean = 0; yStd = 1;
  end
  % do the TRUE model
  [predTrain,predTest,errTrain,errTest,theta] = fitTRUE(yTrain,xTrain,yTest,xTest,VARCoeffs,yMean,yStd);
  save(fullfile(fNameOut,'TRUE.mat'),'predTrain','predTest','errTrain','errTest','theta')
end



% summarise results at the train level
sumMSE(fNameOut)
sumTheta(fNameOut)

%% need to do the same for the inner cv samples for VARL2
for cvI = 1:numFolds
  % load data
  dsName = [fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength),'_',num2str(cvI)]; % datasample name
  load(fullfile('expData/',expName,[dsName,'.mat']),'yTrain','xTrain','yTest','xTest')
  % prepare output folder
  fNameOut = fullfile('expResults/',expName,dsName);
  mkdir(fNameOut)
  % fit and save the simple models
  [predTrain,predTest,errTrain,errTest,theta,time] = fitVARL2(yTrain,xTrain,yTest,xTest,lbdaGrid);
  save(fullfile(fNameOut,'VARL2.mat'),'predTrain','predTest','errTrain','errTest','theta','lbdaGrid','time')
  
  %% summarise results at the cv level
  sumMSE(fNameOut)
  sumTheta(fNameOut)
end



%% find the best cv hyper-parameters and corresponding solutions
% datasample name
dsName = [fileName,num2str(numLagsIn),'_',num2str(trainLength),'_',num2str(testLength)];
% output folder
fNameOut = fullfile('expResults/',expName,dsName);

% initiate output variables
mseTestCV = struct; mseTestCVMean = struct; bestCVLbda = struct;
% get results from all the cv samples
for cvI = 1:numFolds
  load(fullfile([fNameOut,'_',num2str(cvI)],'sumMSE.mat'))
  mseTe(cvI) = mseTest;
end
% sumarise results of all methods across the cv samples
ff = fields(mseTe);
for mIdx = 1:numel(ff)
  metName = ff{mIdx};
  mseTemp = arrayfun(@(x) x.(metName), mseTe, 'UniformOutput',0);
  mseMean = mean([mseTemp{:}],2);
  [~,lbdIdx] = min(mseMean);
  mseTestCV = setfield(mseTestCV,metName,[mseTemp{:}]);
  mseTestCVMean = setfield(mseTestCVMean,metName,mseMean);
  bestCVLbda = setfield(bestCVLbda,metName,lbdIdx);
end
save(fullfile(fNameOut,'sumCV.mat'),'mseTestCV','mseTestCVMean','bestCVLbda')
elapsedTime = toc;

fprintf('END predBaseModels %s time: %6.2f \n',expName,elapsedTime)

end
