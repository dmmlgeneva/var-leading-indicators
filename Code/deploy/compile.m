%%%%%%%%% MG 24/12/2016 - compile functions to deply on clusters %%%%%%%


%% generate standalone applications
mcc -m ../execs/predVARL1.m -o predVARL1 -v -R '-nodisplay, -nojvm' -a ../fitModels/fitVARL1.m

mcc -m ../execs/predVARLG.m -o predVARLG -v -R '-nodisplay, -nojvm' -a ../fitModels/fitVARLG.m

mcc -m ../execs/predSCVAR.m -o predSCVAR -v -R '-nodisplay, -nojvm' -a ../fitModels/fitSCVAR.m ../fitModels/simplexLS.m ../fitModels/simplexProject.m 

mcc -m ../execs/predMCVAR.m -o predMCVAR -v -R '-nodisplay, -nojvm' -a ../fitModels/fitMCVAR.m ../fitModels/simplexLS.m ../fitModels/simplexLSMat.m ../fitModels/simplexProject.m 